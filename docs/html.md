# HTMLの基礎

CSSを学ぶうえで最低限必要となるHTML要素をまとめます。

## html, head, body要素

どんなWebページにも登場するお約束の要素です。Webページを作るときにはまずこの雛形を用意し、`<body>` と `</body>` の間にコンテンツを書いていくことになります。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ここにコンテンツを書く
  </body>
</html>
```

`<meta name="viewport" content="width=device-width, initial-scale=1.0">` は、スマホで閲覧している人にも適切な倍率で表示するための記述です。自分で作ったWebページをスマホで見たときに想定よりも小さく表示された場合は、この記述を忘れていないか確認してみてください。

## span要素

汎用的な要素で、文字や画像を囲んで使います。具体的な用途は続く章で紹介します。次のように `<span>` と `</span>` の間にコンテンツを書いて使います。

```html
<span>コンテンツ</span>
```

また、次のようにspan要素の中にspan要素を入れることもできます。

```html
<span>
  <span>コンテンツ</span>
</span>
```

span要素には、次のような特徴があります。

- 中の**コンテンツに合った幅**をとる
- span要素同士は**横に並ぶ**

次の画像はspan要素を表示した例です。ここではspan要素に緑色の枠線をつけて見やすくしています。

<img :src="$withBase('/html-span.png')" alt="span要素のサンプル" width="375" style="border: 1px solid #eaecef">

<a href="../samples/html-span.html" target="_blank">span要素のサンプル</a>

## div要素

span要素と同様に汎用的な要素で、文字や画像を囲んで使います。具体的な用途は続く章で紹介します。次のように `<div>` と `</div>` の間にコンテンツを書いて使います。

```html
<div>コンテンツ</div>
```

また、次のようにdiv要素やspan要素を入れ子にして使うこともできます。

```html
<div>
  <div>コンテンツ</div>
</div>
```

```html
<div>
  <span>コンテンツ</span>
</div>
```

ただし、次のようにspan要素の中にdiv要素を配置することはできません。

```html
<span>
  <div>コンテンツ</div>
</span>
```

div要素は、次のような特徴があります。

- **横いっぱいの幅**をとる
- div要素同士は**縦に並ぶ**

次の画像はdiv要素を表示した例です。ここではdiv要素に緑色の枠線をつけて見やすくしています。

<img :src="$withBase('/html-div.png')" alt="div要素のサンプル" width="375" style="border: 1px solid #eaecef">

<a href="../samples/html-div.html" target="_blank">div要素のサンプル</a>

## img要素

画像を表示するために使う要素で、次のように `<img />` の中にsrc属性とalt属性を指定して使います。

```html
<img src="画像ファイルの場所" alt="画像の説明" />
```

<a href="../samples/html-img.html" target="_blank">img要素のサンプル</a>

### 画像ファイルの場所の指定方法

あるファイルの場所を表現するときは、ディレクトリを `/` 区切りで書きます。例をあげると、`../static/sample0.jpg` というような表現です。

これは `../`, `static/`, `sample0.jpg` という部分から構成されていて、これらはそれぞれ「ひとつ上のディレクトリへ上がる」「そのディレクトリにあるstaticディレクトリへ入る」「そのディレクトリにあるsample0.jpg」という意味を表しています。

具体的に下のようなディレクトリ構造で考えてみましょう。

```
web/
├─ static/
│  ├─ sample0.jpg
│  ├─ sample1.jpg
│  └─ sample2.jpg
└─ page/
   ├─ page0.html
   ├─ page1.html
   └─ page2.html
```

page0.htmlにsample0.jpgを表示したいとします。このときには、次のように考えて画像ファイルの場所を指定します。

1. 現在のディレクトリを確認する（page0.htmlはpageディレクトリにある）
1. pageディレクトリからひとつ上のwebディレクトリへ上がる（`../`）
1. webディレクトリにあるstaticディレクトリへ入る（`static/`）
1. staticディレクトリにあるsample0.jpgを表示したい（`sample0.jpg`）
1. `../`, `static/`, `sample0.jpg` をつなげて `../static/sample0.jpg`
1. img要素のsrc属性に記述する（`<img src="../static/sample0.jpg" alt="サンプルの画像" />`）

## style要素

CSSを記述するために使う要素です。`<style>` と `</style>` の間にCSSを書いて使います。

```html
<style>
  /* ここに CSS を書く */
</style>
```

::: tip
HTMLには他にも多くの要素があります。

適切な要素を使うことでコンテンツを正しく表現でき、伝わりやすく使いやすいWebサイトを作ることができます。

要素の一覧は、[HTML 要素リファレンス - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/ja/docs/Web/HTML/Element) などを参照してください。
:::
