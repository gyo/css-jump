# PCのレイアウトを作る

さきほど作ったスタイルを、PCで閲覧した際に異なるレイアウトになるように改修していきます。

PCで閲覧しているかどうかはウィンドウの幅で区別します。指定した幅のときにのみ適用されるようなスタイルを記述することで見た目を切り替えます。

ここでは、PCで閲覧していると判定する基準を「768px以上のウィンドウ幅で閲覧している」としましょう。768pxは、iPadを縦向きにしたときの横幅をもとにした値です。

768px以上のウィンドウ幅でのみ適用されるスタイルを定義するには、`@media` を使い次のように書きます。

```css
@media screen and (min-width: 768px) {
  /* 768px以上のウィンドウ幅でのみ適用されるスタイル */
}
```

## 構造をイメージする

今回作るレイアウトの構造をもう一度確認しておきましょう。

<img :src="$withBase('/responsive-layout-desktop-structure.png')" alt="レイアウトのイメージ" width="480" style="border: 1px solid #eaecef">

## メディアのレイアウトを作る

まずは緑色の四角形と水色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-desktop-skeleton-2.png')" alt="レイアウトの構造のイメージ" width="480" style="border: 1px solid #eaecef">

スマホ向けのレイアウトは、このようになっていました。

<img :src="$withBase('/responsive-layout-mobile-skeleton-2.png')" alt="スマホのレイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

つまり、スマホ向けでは横並びになっていたものを、縦に並ぶようにすればよさそうです。

この部分を横並びにしているのはコードの次の部分です。

```css{2}
.information {
  display: flex;
  height: calc(
    var(--spacing-1) * 2 + var(--font-size-large-1) * var(--line-height-small) +
      var(--spacing-1) + var(--font-size-small-2) * var(--line-height-base) * 2
  ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
  background: var(--color-gray-light);
}
```

また、次の部分もサイズを決めるうえで関連してきています。

```css{3-6,11,12}
.information {
  display: flex;
  height: calc(
    var(--spacing-1) * 2 + var(--font-size-large-1) * var(--line-height-small) +
      var(--spacing-1) + var(--font-size-small-2) * var(--line-height-base) * 2
  ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
  background: var(--color-gray-light);
}

.information__image {
  width: calc(var(--spacing-unit) * 12);
  height: 100%;
  object-fit: cover;
}
```

まず、`display: flex;` による横並びを解除するために、`display: block;` で上書きします。

```html{11-15}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        .information {
          display: block;
        }
      }
    </style>
  </body>
</html>
```

また、サイズを決める部分も上書きしていきます。カードの高さは `height: auto;` を指定することでデフォルトの挙動に戻します。そして画像の幅はカードの幅と同じにするために `width: 100%;`を、画像の高さは固定で `height: calc(var(--spacing-unit) * 24);` を指定することにします。

```html{14,17-20}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        .information {
          display: block;
          height: auto;
        }

        .information__image {
          width: 100%;
          height: calc(var(--spacing-unit) * 24);
        }
      }
    </style>
  </body>
</html>
```

ここまでで、一度ウィンドウの幅を広げたり狭めたりしてみてください。幅が768pxを超えたところでレイアウトが切り替わるのを確認できます。

次に水色の四角形と黄緑色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-desktop-skeleton-3.png')" alt="レイアウトの構造のイメージ" width="480" style="border: 1px solid #eaecef">

スマホ向けのレイアウトは、このようになっていました。

<img :src="$withBase('/responsive-layout-mobile-skeleton-3.png')" alt="スマホのレイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

つまり、スマホ向けでは上下の中央に2つ配置されていたものを、単に縦に並ぶようにすればよさそうです。

上下の中央に2つ配置しているのはコードの次の部分です。

```css{4-6}
.information__text-area {
  flex-grow: 1;
  padding: var(--spacing-1);
  display: flex;
  flex-direction: column;
  justify-content: center;
}
```

ここでも `display: flex;` を解除するために、`display: block;` で上書きします。`flex-direction` と `justify-content` は `display: flex;` と合わせて指定されたときのみ効果を発揮するものなので、これら2つを改めて上書きする必要はありません。

```html{14-16}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        /* ... */

        .information__text-area {
          display: block;
        }
      }
    </style>
  </body>
</html>
```

また今回のレイアウトでは、本文は2行ではなく3行で省略するようにします。そして、本文が3行未満だったときにも高さを揃えられるように、本文のエリアの高さを本文3行分で固定しておきましょう。

```html{14-19}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        /* ... */

        .information__body {
          height: calc(
            var(--font-size-small-2) * var(--line-height-base) * 3
          ); /* .information__body height */
          -webkit-line-clamp: 3;
        }
      }
    </style>
  </body>
</html>
```

## リストのレイアウトを作る

最後に、赤色の四角形とオレンジ色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-desktop-skeleton-1.png')" alt="レイアウトの構造のイメージ" width="480" style="border: 1px solid #eaecef">

スマホ向けのレイアウトは、このようになっていました。

<img :src="$withBase('/responsive-layout-mobile-skeleton-1.png')" alt="スマホのレイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

シンプルに縦に並んでいたものを、`flex-wrap: wrap;` を使って3つずつ横並びにすればよさそうです。

```html{12-19}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        .information-list {
          display: flex;
          flex-wrap: wrap;
        }

        .information-list__item {
          width: calc(100% / 3);
        }

        /* ... */
      }
    </style>
  </body>
</html>
```

カード同士の余白も設定していきます。[「画像のグリッドをつくる」](/layout-content/image-grid.md#画像のグリッドを作る) では `:nth-child(3n)` と `:nth-last-child(-n + 3)` を使って余白を設定しました。

`:nth-last-child(-n + 3)` は「後ろから1, 2, 3番目の要素」を指定するものですが、今回は要素が8個なので、最終行には2個しか要素が存在せず、最終行の要素の個数と一致しません。
`:nth-last-child(-n + 2)` としてもよいのですが、これでは要素の個数の変化に対応しきれないので、今回は「ネガティブマージン」を利用した別の方法を使います。

カード同士の余白に影響を与えているコードは次の部分です。

```css
.information-list__item {
  margin-bottom: var(--spacing-2);
}

.information-list__item:last-child {
  margin-bottom: 0;
}
```

カードの間には全て `var(--spacing-2)` の余白を設定したいので、まずは次のように余白設定を上書きしましょう。

```html{15-16,19-21}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        /* ... */

        .information-list__item {
          width: calc(100% / 3 - var(--spacing-2));
          margin-right: var(--spacing-2);
        }

        .information-list__item:last-child {
          margin-bottom: var(--spacing-2);
        }

        /* ... */
      }
    </style>
  </body>
</html>
```

この状態の問題点は2つあります。

- `margin-right` が3, 6個目のカードに設定されており、右端の余白が余計についてしまっている
- `margin-bottom` が7, 8個目のカードに設定されており、下端の余白が余計についてしまっている

これらの問題点を解消するために、「ネガティブマージン」を利用します。`margin-right`, `margin-bottom` を設定した `.information-list__item` の親要素である `.information-list` に、`margin-right: calc(var(--spacing-2) * -1);`, `margin-bottom: calc(var(--spacing-2) * -1);` を指定します。

ポイントは `* -1` の部分で、marginにマイナスの値を指定することによって、子要素が生み出した余計な余白を無視しています。

```html{15-16}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 768px) {
        .information-list {
          display: flex;
          flex-wrap: wrap;
          margin-right: calc(var(--spacing-2) * -1);
          margin-bottom: calc(var(--spacing-2) * -1);
        }

        /* ... */
      }
    </style>
  </body>
</html>
```

最後に、幅がさらに広がったときに3カラムから5カラムになるような指定をしておきます。
「1024px以上のウィンドウ幅で閲覧している」場合を追加しましょう。1024pxは、iPadを横向きにしたときの横幅をもとにした値です。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @media screen and (min-width: 1024px) {
        .information-list__item {
          width: calc(100% / 5 - var(--spacing-2));
        }
      }
    </style>
  </body>
</html>
```

<a href="../../samples/responsive-layout-desktop-layout.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したメディアクエリは以下のとおりです。

- [@media - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/@media)
  :::
