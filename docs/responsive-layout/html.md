# HTMLを組む

## 構造をイメージする

レスポンシブなデザインを実現する場合でも、これまでと同様にまずはHTMLの構造から考えていきます。

今回作るのは次のようなレイアウトです。画像の左側がスマホ幅におけるレイアウト、画像の右側はタブレット以上の幅におけるレイアウトです。

<img :src="$withBase('/responsive-layout-image.png')" alt="レイアウトのイメージ" width="480" style="border: 1px solid #eaecef">

これまでと同様に、レイアウトを構成する四角形をイメージしてみてください。

<img :src="$withBase('/responsive-layout-structure.png')" alt="レイアウトの構造のイメージ" width="480" style="border: 1px solid #eaecef">

構造のみを表示すると次のようになります。

<img :src="$withBase('/responsive-layout-skeleton-1.png')" alt="レイアウトの構造を抽出したイメージ" width="480" style="border: 1px solid #eaecef">

スマホの場合もPCの場合も、赤色の四角形の中にオレンジ色の四角形が8個並んでいるという構造は同じです。レイアウトは、スマホの場合は縦に、PCの場合は左上から右方向に3つずつ折り返して並んでいます。

<img :src="$withBase('/responsive-layout-skeleton-2.png')" alt="レイアウトの構造を抽出したイメージ" width="480" style="border: 1px solid #eaecef">

オレンジ色の四角形の中には緑色の四角形があり、緑色の四角形の中には水色の四角形が2つ並んでいます。このとき、スマホの場合は横に、PCの場合は縦に並んでいるという違いがあります。

<img :src="$withBase('/responsive-layout-skeleton-3.png')" alt="レイアウトの構造を抽出したイメージ" width="480" style="border: 1px solid #eaecef">

最後に、2つ目の水色の四角形の中には黄緑色の四角形が2つ並んでいます。スマホの場合は上下の中央に、PCの場合は上下にレイアウトされています。

<img :src="$withBase('/responsive-layout-skeleton-4.png')" alt="レイアウトの構造を抽出したイメージ" width="480" style="border: 1px solid #eaecef">

::: tip
スマホとPCでレイアウトは違っていても、構造自体は同じであることが確認できたでしょうか。

レスポンシブデザインは基本的に、同じ構造のHTMLに対して複数のスタイリングを行うことで見た目の切り替えを実現するテクニックです。

構造の異なるHTMLを使うことも不可能ではありませんが、HTMLのセマンティクス（コンテンツの意味や役割）などの観点からも、構造が同じであることが望ましいです。
:::

## メディア（カード）のHTMLを1つ作る

構造を参考に、HTMLを組んでいきましょう。

```html{9-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="赤">
        <div class="オレンジ">
          <div class="緑">
            <div class="水色">
              <img src="../static/sample1.jpg" alt="サンプル" />
            </div>
            <div class="水色">
              <div class="黄緑">
                ポラーノの広場
              </div>
              <div class="黄緑">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

## HTMLに名前をつける

続いて、要素にclass名を振っていきましょう。

```html{9,10,11,12,14,19,20,23}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-list">
        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample1.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

## メディア（カード）のリスト（カラム）のHTMLを作る

あらかじめランダムな画像、テキストサイズにしておきます。

```html{30-168}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-list">
        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample1.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample2.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample3.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample4.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample5.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample6.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                あのイーハトーヴォのすきとおった風
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample7.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            <div class="information__image-area">
              <img
                class="information__image"
                src="../static/sample8.jpg"
                alt="サンプル"
              />
            </div>
            <div class="information__text-area">
              <div class="information__heading">
                ポラーノの広場
              </div>
              <div class="information__body">
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

<a href="../../samples/responsive-layout-html.html" target="_blank">ここまでの成果物</a>
