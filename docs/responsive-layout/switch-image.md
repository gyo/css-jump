# スマホとPCで画像を切り替える

レスポンシブデザインでは基本的に、スマホとPCで同じHTMLを利用します。そしてimg要素のsrc属性は、1つの画像しか描画できません。

スマホとPCで同じ画像を利用する場合、次のような課題に遭遇することがあります。

- 画像を表示する領域の縦横比やサイズが違うため、両方に最適化した見た目を作れない。
- 小さいディスプレイに表示するときにも、大きいディスプレイで表示できるだけの画像サイズが必要になってしまう。

これらの課題に対し、picture要素とsource要素を使って解決する方法を紹介します。

## picture要素

768px以上のウィンドウ幅でのみサイズの大きな画像を読み込む場合には、picture要素を使って次のように書きます。

```html{1,2,4}
<picture>
  <source srcset="大きな画像ファイルの場所" media="(min-width: 768px)" />
  <img src="画像ファイルの場所" alt="画像の説明" />
</picture>
```

このように書くことで、小さいディスプレイでは小さな画像、大きいディスプレイでは大きな画像が読み込まれるようになり、表示する画像を制御できます。また、通信量やページ読み込み時間も節約できます。

## 画像の切り替えを適用する

サンプルの画像はこちらです。

<a href="../../static/sample1-half.jpg" target="_blank">sample1-half.jpg</a>, <a href="../../static/sample2-half.jpg" target="_blank">sample2-half.jpg</a>, <a href="../../static/sample3-half.jpg" target="_blank">sample3-half.jpg</a>, <a href="../../static/sample4-half.jpg" target="_blank">sample4-half.jpg</a>, <a href="../../static/sample5-half.jpg" target="_blank">sample5-half.jpg</a>, <a href="../../static/sample6-half.jpg" target="_blank">sample6-half.jpg</a>, <a href="../../static/sample7-half.jpg" target="_blank">sample7-half.jpg</a>, <a href="../../static/sample8-half.jpg" target="_blank">sample8-half.jpg</a>, <a href="../../static/sample9-half.jpg" target="_blank">sample9-half.jpg</a>

画像をダウンロードし、`css-jump/static` ディレクトリの中に保存してください。

768px以上のウィンドウ幅ではsampleX.jpgを、768px未満のウィンドウ幅ではsampleX-half.jpgを読み込み、表示するようにソースコードを変更します。ここではCSSの変更はありません。

```html{14-24,35-45,56-66,77-87,98-108,119-129,140-150,161-171}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-area">
        <div class="information-list">
          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample1.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample1-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample2.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample2-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample3.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample3-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample4.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample4-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample5.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample5-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample6.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample6-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample7.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample7-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample8.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample8-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                ...
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

::: tip
img要素のsrcset属性とsizes属性を利用した画像の切り替え方法もあり、実はディスプレイごとの解像度の制御においてはそちらが推奨されています。

picture要素とsource要素のmedia属性を使った指定は強制力が強いため、ユーザーによる設定を無視してしまったり、ブラウザ等のユーザーエージェントによる最適化の恩恵を受けられなってしまうことがあります。逆に言うと、指定した幅ごとに必ず意図した画像を表示させる必要のあるときには、picture要素を使った切り替えが有効です。
:::

<a href="../../samples/responsive-layout-goal.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したHTML要素は以下のとおりです。

- [\<picture\>: 画像要素 - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/ja/docs/Web/HTML/Element/picture)
- [\<source\> - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/ja/docs/Web/HTML/Element/source)
  :::
