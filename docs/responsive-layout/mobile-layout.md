# スマホのレイアウトを作る

レスポンシブなデザインを制作する際には、「モバイルファースト」の考え方でスマホ向けのデザインを先に作ることが多いです。

## 構造をイメージする

今回作るレイアウトの構造をもう一度確認しておきましょう。

<img :src="$withBase('/responsive-layout-mobile-structure.png')" alt="レイアウトのイメージ" width="200" style="border: 1px solid #eaecef">

## メディアのレイアウトを作る

まずは緑色の四角形と水色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-mobile-skeleton-2.png')" alt="レイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

緑色の四角形の中には水色の四角形が2つ横に並んでいます。
また [「メディアのリストをつくる」](/layout-content/media-list.md) と同様に、左の水色の四角形は幅が決まっており、右の水色の四角形は残りの幅を全て埋めるようにレイアウトするとしましょう。

```html{11-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
      }

      .information__image-area {
        flex-shrink: 0;
      }

      .information__image {
        width: calc(var(--spacing-unit) * 12);
      }

      .information__text-area {
        flex-grow: 1;
      }
    </style>
  </body>
</html>
```

また、背景色と画像の高さの設定もしておきましょう。

```html{13,22-23}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        background: var(--color-gray-light);
      }

      .information__image-area {
        flex-shrink: 0;
      }

      .information__image {
        width: calc(var(--spacing-unit) * 12);
        height: 100%;
        object-fit: cover;
      }

      .information__text-area {
        flex-grow: 1;
      }
    </style>
  </body>
</html>
```

次に水色の四角形と黄緑色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-mobile-skeleton-3.png')" alt="レイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

まずは見出しと本文のスタイルを設定しておきましょう。

```html{11-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information__heading {
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
      }

      .information__body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
      }
    </style>
  </body>
</html>
```

余白も設定しておきましょう。

```html{15,19,20,24,34,38,39,43,53,57,58,62,72,76,77,81,91,95,96,100,110,114,115,119,129,133,134,138,148,152,153,157,173,179-184}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-list">
        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="information-list__item">
          <div class="information">
            ...

            <div class="information__text-area">
              <div class="information__heading-area">
                <div class="information__heading">
                  ポラーノの広場
                </div>
              </div>
              <div class="information__body-area">
                <div class="information__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      Footer
    </div>

    <style>
      /* ... */

      .information__text-area {
        flex-grow: 1;
        padding: var(--spacing-1);
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      .information__heading-area {
        margin-bottom: var(--spacing-1);
      }

      .information__body-area {
      }

      /* ... */
    </style>
  </body>
</html>
```

また今回のレイアウトでは、見出しは1行、本文は2行で省略するようにします。メディアの高さも、見やすさのために固定値で揃えることにしましょう。

このとき、メディアの高さはテキストが取りうる最大の高さに固定することにします。つまり、テキストエリアの上下の `padding`、見出しの高さ、見出しと本文の間の余白、本文が2行のときの高さを足した値にします。

<img :src="$withBase('/responsive-layout-mobile-item-height.png')" alt="メディアの高さのイメージ" width="480" style="border: 1px solid #eaecef">

高さの計算式が複雑なので、コメントを残しておくとよいでしょう。CSSは、`/* ここにコメント */` という形式でメモを残すことができます。このメモはスタイリングには影響しないので、制作意図や注意点を書いておくことができます。

```html{13-19,26,32-34,38,44-46}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
      }

      /* ... */

      .information__heading {
        display: -webkit-box;
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
      }

      .information__body {
        display: -webkit-box;
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
      }
    </style>
  </body>
</html>
```

最後に、黄緑色の四角形が水色の四角形のなかで上下の中央に2つ配置されるようにしましょう。

```html{14-16}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information__text-area {
        flex-grow: 1;
        padding: var(--spacing-1);
        display: flex;
        flex-direction: column;
        justify-content: center;
      }
    </style>
  </body>
</html>
```

## リストのレイアウトを作る

最後に、赤色の四角形とオレンジ色の四角形の部分のレイアウトを作っていきます。

<img :src="$withBase('/responsive-layout-mobile-skeleton-1.png')" alt="レイアウトの構造のイメージ" width="200" style="border: 1px solid #eaecef">

ここは、要素ごとの間に余白を設ければよいでしょう。

```html{11-17}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information-list__item {
        margin-bottom: var(--spacing-2);
      }

      .information-list__item:last-child {
        margin-bottom: 0;
      }

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
      }

      /* ... */
    </style>
  </body>
</html>
```

最後に、全体を囲む要素に `margin` を設定して完成です。

```html{9,13,26-28}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-area">
        <div class="information-list">
          ...
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .main {
        padding-top: var(--spacing-5);
        padding-left: var(--spacing-2);
        padding-right: var(--spacing-2);
      }

      .information-area {
        margin-bottom: var(--spacing-5);
      }

      .footer {
        display: flex;
        justify-content: center;
        padding: var(--spacing-1) var(--spacing-2);
        background: var(--color-theme-dimmed);
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      /* ... */
    </style>
  </body>
</html>
```

<a href="../../samples/responsive-layout-mobile-layout.html" target="_blank">ここまでの成果物</a>
