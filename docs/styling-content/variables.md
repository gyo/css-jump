# 値を管理する

現時点のCSSはこのようになっています。

```html
<style>
  .heading {
    font-family: serif;
    font-weight: bold;
    font-size: 2.666rem;
    line-height: 1.2;
    color: hsl(244, 16%, 18%);
  }
  .body {
    font-family: sans-serif;
    font-weight: normal;
    font-size: 1rem;
    line-height: 1.5;
    color: hsl(244, 16%, 18%);
  }
  .button {
    display: inline-block;
    padding: 1em 2em;
    border-radius: 0.25em;
    background: hsl(290, 18%, 37%);
    font-family: sans-serif;
    font-weight: bold;
    font-size: 1rem;
    line-height: 1;
    color: hsl(244, 16%, 98%);
  }
  .image {
    max-width: 100%;
  }
</style>
```

文字サイズや色など、すべて値として書かれていますが、これでは複数人で制作する際にコミュニケーションをとりにくくなってしまっています。また、時間が経ったあとでデザインの意図を読み取るのも難しくなっていまいます。

文字サイズや色といった「デザイントークン」は名前をつけて管理しておくとよいでしょう。デザイントークンに名前をつける仕組みをカスタムプロパティといいます。

CSSの中に `:root {}` という場所を用意して、その中に `--名前: 値;` という形で定義します。定義したものは `var(--名前)` という形で利用します。

先ほどのコードをカスタムプロパティを使って書き直すと次のようになります。

```html{2-10,14-16,21-23,29,32,34}
<style>
  :root {
    --color-black: hsl(244, 16%, 18%);
    --color-white: hsl(244, 16%, 98%);
    --color-accent: hsl(290, 18%, 37%);
    --font-size-base: 1rem;
    --font-size-heading-level-1: 2.666rem;
    --line-height-small: 1.2;
    --line-height-base: 1.5;
  }
  .heading {
    font-family: serif;
    font-weight: bold;
    font-size: var(--font-size-heading-level-1);
    line-height: var(--line-height-small);
    color: var(--color-black);
  }
  .body {
    font-family: sans-serif;
    font-weight: normal;
    font-size: var(--font-size-base);
    line-height: var(--line-height-base);
    color: var(--color-black);
  }
  .button {
    display: inline-block;
    padding: 1em 2em;
    border-radius: 0.25em;
    background: var(--color-accent);
    font-family: sans-serif;
    font-weight: bold;
    font-size: var(--font-size-base);
    line-height: 1;
    color: var(--color-white);
  }
  .image {
    max-width: 100%;
  }
</style>
```

それぞれの値がちゃんと意味のある名前を持つことで、デザインの意図が表現、伝達しやすくなっていることが分かります。

<a href="../../samples/styling-content-variables.html" target="_blank">ここまでの成果物</a>

::: tip
カスタムプロパティについての詳しい説明は次の記事などを確認してください。

[CSS カスタムプロパティ (変数) の使用 - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/Using_CSS_custom_properties)
:::
