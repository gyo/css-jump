# 画像のサイズとアスペクト比を調整する

次に、画像の幅や高さを調整して、どんなデバイスでの表示にも耐えられるようにしましょう。

画像にはヒーローイメージとしての役割を期待しているとして、PCのような幅が広い端末やスマホのような幅が狭く縦に長い端末など、どんな端末で閲覧されてもある程度の見た目を保証できるようにすることが目標です。

## 現時点の表示を確認する

まずは現時点の表示を確認しておきましょう。

<a href="../../samples/styling-content-spacing.html" target="_blank">ここまでの成果物</a>

スマホ（375x667）で表示した場合は、次のようになっているはずです。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-1.png')" alt="ここまでの実装をスマホで確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

PC（1024x768）で表示した場合、次のようになっているはずです。

<img :src="$withBase('/styling-content-image-aspect-ratio-desktop-1.png')" alt="ここまでの実装を PC で確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

ウィンドウの幅よりも画像の幅が小さいため、画面の途中で画像が途切れてしまっています。
画像をヒーローイメージとして使う場合、これでは役割を果たせません。

最後に、忘れがちですがスマホを横向きにしたときの表示も確認してみましょう。

このとき、次のような表示になっているはずです。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-landscape-1.png')" alt="ここまでの実装を横向きにしたスマホで確認したときのキャプチャ" width="320" style="border: 1px solid #eaecef">

画面の途中で画像が途切れているだけでなく、コンテンツがほとんど見えません。

これではファーストビューで大切な情報を伝えられません。

## 対応方針を決める

次のような方針で対応することにします。

- 画像の幅はウィンドウの幅100% にする
- 画像の高さは最大でもウィンドウの高さの30% にする

## 画像の幅をウィンドウの幅100% にする

幅を横幅いっぱいにするには、基本的には `width: 100%;` を指定することになります。

`%` 単位を使った指定は親要素の幅に対する割合になるので、最初に、画像とそれに関連する要素の構造を確認しておきましょう。

次のような、body要素の中にdiv要素があり、その中にimg要素がある構造になっています。

```html
<body>
  <div class="image-area">
    <img class="image" src="../static/sample0.jpg" alt="サンプル" />
  </div>
</body>
```

今回の場合、body要素もdiv要素もウィンドウの幅いっぱいに表示される要素なので、画像をウィンドウの幅にするには、img要素に `width: 100%;` を指定するだけで十分です。

```html{12}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .image {
        width: 100%;
      }
    </style>
  </body>
</html>
```

::: tip
ためしに、画像や、画像の親要素の幅を変化させてみてください。`%` が親要素の幅に対する指定だということが確認できると思います。

```css
.image-area {
  margin-bottom: var(--spacing-2);
  width: 50%;
}

.image {
  width: 50%;
}
```

:::

## 画像の高さを最大でもウィンドウの高さの30% にする

ここまでの実装を確認してみましょう。

スマホの場合は次のようになっています。とくに問題なさそうです。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-2.png')" alt="ここまでの実装をスマホで確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

PCの場合は次のようになっています。画像の幅がウィンドウの幅いっぱいになったため、コンテンツがほとんど見えなくなってしまっています。

<img :src="$withBase('/styling-content-image-aspect-ratio-desktop-2.png')" alt="ここまでの実装を PC で確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

スマホを横向きにしたときは次のようになっています。PC表示のときと同様ですが、こちらはコンテンツが全く見えなくなってしまっています。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-landscape-2.png')" alt="ここまでの実装を横向きにしたスマホで確認したときのキャプチャ" width="320" style="border: 1px solid #eaecef">

縦よりも横の幅が広いウィンドウで閲覧したときに問題が残っています。

画像の高さを最大でもウィンドウの高さの30% にするには、`max-height: 30vh;` を指定します。

`vh` は `viewport's height` という意味で、`1vh` でウィンドウの高さの1% になります。

```html{13}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .image {
        width: 100%;
        max-height: 30vh;
      }
    </style>
  </body>
</html>
```

表示を確認してみます。

スマホの場合はこのようになっています。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-3.png')" alt="ここまでの実装をスマホで確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

デスクトップの場合はこのようになっています。

<img :src="$withBase('/styling-content-image-aspect-ratio-desktop-3.png')" alt="ここまでの実装を PC で確認したときのキャプチャ" height="280" style="border: 1px solid #eaecef">

スマホを横向きにした場合はこのようになっています。

<img :src="$withBase('/styling-content-image-aspect-ratio-iphone-landscape-3.png')" alt="ここまでの実装を横向きにしたスマホで確認したときのキャプチャ" width="320" style="border: 1px solid #eaecef">

画像の高さを制限することはできましたが、画像のアスペクト比が崩れていることが分かります。

画像のアスペクト比を維持するために、今回は次のような方針で行くことにします。

## 表示エリアを埋めるように画像を拡大縮小する

画像をアスペクト比を維持したまま拡大縮小するには、`object-fit: contain;` もしくは `object-fit: cover;` という指定を使います。画像全体を常に表示したいときは `object-fit: contain;`、表示エリアを埋めたいときは `object-fit: cover;` と使い分けます。

今回はヒーローイメージとして常に表示エリアを埋めてほしいので、`object-fit: cover;` を使いましょう。

```html{14}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .image {
        width: 100%;
        max-height: 30vh;
        object-fit: cover;
      }
    </style>
  </body>
</html>
```

これで、どんなデバイスでもヒーローイメージとして機能するようなスタイリングを行うことができました。

<a href="../../samples/styling-content-goal.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [width - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/width)
- [max-height - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/max-height)
- [object-fit - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/object-fit)
  :::
