# 画像を表示する

## 作るものを確認する

今回作るのはこの部分です。

<img :src="$withBase('/styling-content-image-goal.png')" alt="今回作る部分" width="375" style="border: 1px solid #eaecef">

## 画像を用意する

こちらにサンプルの画像があります。

<a href="../../static/sample0.jpg" target="_blank">sample0.jpg</a>

画像をダウンロードしたら `css-jump` ディレクトリの中に `static` ディレクトリを作成し、その中に `sample0.jpg` を移動してください。

<img :src="$withBase('/styling-content-image-create-file.png')" alt="今回作る部分" width="375" style="border: 1px solid #eaecef">

## HTMLを書く

画像を表示するには、[img 要素](/html.md#img要素)を使います。

先ほど配置した画像を、次のように表示してみましょう。

```html{6}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <img src="../static/sample0.jpg" alt="サンプル" />

    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <span class="button">ボタン</span>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
        display: inline-block;
        padding: 1em 2em;
        border-radius: 0.25em;
        background: hsl(290, 18%, 37%);
        font-family: sans-serif;
        font-weight: bold;
        font-size: 1rem;
        line-height: 1;
        color: hsl(244, 16%, 98%);
      }
    </style>
  </body>
</html>
```

画像のサイズを指定していないので、現時点ではどんな幅のデバイスで閲覧しても同じ大きさで表示されます。幅が狭いデバイスで見ているときは、横スクロールをしないと画像全体を見れない状態です。

画像の横幅がウィンドウの横幅を超えないように、`max-width` を指定しておきましょう。

```html{6,42-44}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <img class="image" src="../static/sample0.jpg" alt="サンプル" />

    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <span class="button">ボタン</span>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
        display: inline-block;
        padding: 1em 2em;
        border-radius: 0.25em;
        background: hsl(290, 18%, 37%);
        font-family: sans-serif;
        font-weight: bold;
        font-size: 1rem;
        line-height: 1;
        color: hsl(244, 16%, 98%);
      }
      .image {
        max-width: 100%;
      }
    </style>
  </body>
</html>
```

ウィンドウの幅を狭くしても画像がはみ出さなくなっていれば完成です。

<a href="../../samples/styling-content-image.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [max-width - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/max-width)
  :::
