# 余白をつける

これまで作ってきた「画像、見出し、本文、ボタン」のパーツの配置を調整しましょう。

具体的には、次のような対応をしていきます。

- 全体の外側にある余白を消す
- 画像の下に余白を設ける
- 見出しの左右と下に余白を設ける
- 本文の左右と下に余白を設ける
- ボタンの左右に余白を設ける

## marginについて

要素の外側の余白を設定するには、`margin` を使います。

`margin` は `margin-top`, `margin-right`, `margin-bottom`, `margin-left` の4方向に設定でき、それぞれ上方向、右方向、下方向、左方向の余白を設定するのに使います。

## 全体の余白を消す

次の画像で赤く色をつけている部分が消そうとしている余白です。

<img :src="$withBase('/styling-content-spacing-body.png')" alt="body要素の余白" width="375" style="border: 1px solid #eaecef">

まず、定義していないのに余白がついてしまう原因を説明します。

これはGoogle Chromeの仕様で、デフォルトでbody要素の周りに8pxの `margin` をとるようになっているためです。

余白を削除したいときは、`margin-top: 0px;`, `margin-right: 0px;`, `margin-bottom: 0px;`, `margin-left: 0px;` でこれを上書きすることになります。

```html{18-23}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      :root {
        --color-black: hsl(244, 16%, 18%);
        --color-white: hsl(244, 16%, 98%);
        --color-accent: hsl(290, 18%, 37%);
        --font-size-base: 1rem;
        --font-size-heading-level-1: 2.666rem;
        --line-height-small: 1.2;
        --line-height-base: 1.5;
      }
      body {
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
      }

      /* ... */
    </style>
  </body>
</html>
```

::: tip
CSSを用意するときには名前の前に `.` をつけるのがルールだと説明しましたが、ここではつけていません。

じつは `.` をつける必要があるのはHTML要素に `class="名前"` という形で名前をつけたときのみであり、 bodyという "要素" に対してCSSを書く場合にはこの `.` をつける必要はなくなります。

詳しく知りたい方は、[CSS セレクター - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/CSS_Selectors) などを参照してください。
:::

::: tip
`padding` と同様に、4方向全てに同じ値の `margin` を指定するときは `margin: 0px;` という省略した形を使うことができます。

また、値に `0` を指定するときはどんな単位を指定しても同じなので、単位を省略することも可能です。

そのため、次のふたつは同じ意味になります。

```css
margin-top: 0px;
margin-right: 0px;
margin-bottom: 0px;
margin-left: 0px;
```

```css
margin: 0;
```

:::

## 余白をつけたい要素をdivで囲む

要素間に設定したい余白は次のようなイメージです。

<img :src="$withBase('/styling-content-spacing-elements.png')" alt="要素間の余白" width="375" style="border: 1px solid #eaecef">

余白をつけるときには、対象の要素をdiv要素で囲み、囲んだdiv要素に `margin` を設定します。

まずは、対象要素をdiv要素で囲み、`class="名前"` で名前をつけましょう。

```html{6,8,10,12,14,18,20,22}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="image-area">
      <img class="image" src="../static/sample0.jpg" alt="サンプル" />
    </div>

    <div class="heading-area">
      <div class="heading">ポラーノの広場</div>
    </div>

    <div class="body-area">
      <div class="body">
        あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
      </div>
    </div>

    <div class="button-area">
      <span class="button">ボタン</span>
    </div>

    ...
  </body>
</html>
```

## marginを設定する

それぞれの領域に余白を設定していきます。

パーツ全体のサイズを調和させるため、文字サイズと同じように `rem` を単位とするとよいでしょう。

```html{15-31}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      body {
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
      }
      .image-area {
        margin-bottom: 1rem;
      }
      .heading-area {
        margin-left: 1rem;
        margin-right: 1rem;
        margin-bottom: 1rem;
      }
      .body-area {
        margin-left: 1rem;
        margin-right: 1rem;
        margin-bottom: 2.5rem;
      }
      .button-area {
        margin-left: 1rem;
        margin-right: 1rem;
      }

      /* ... */
    </style>
  </body>
</html>
```

## 余白サイズを管理する

文字サイズや色と同様に、余白も名前をつけてデザイントークンとして管理します。

ここではフィボナッチ数列を利用して5つのサイズを用意することとしました。

```html{17-21,30,33-35,38-40,43-44}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      :root {
        --color-black: hsl(244, 16%, 18%);
        --color-white: hsl(244, 16%, 98%);
        --color-accent: hsl(290, 18%, 37%);
        --font-size-base: 1rem;
        --font-size-heading-level-1: 2.666rem;
        --line-height-small: 1.2;
        --line-height-base: 1.5;
        --spacing-1: 0.5rem;
        --spacing-2: 1rem;
        --spacing-3: 1.5rem;
        --spacing-5: 2.5rem;
        --spacing-8: 4rem;
      }
      body {
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
      }
      .image-area {
        margin-bottom: var(--spacing-2);
      }
      .heading-area {
        margin-left: var(--spacing-2);
        margin-right: var(--spacing-2);
        margin-bottom: var(--spacing-2);
      }
      .body-area {
        margin-left: var(--spacing-2);
        margin-right: var(--spacing-2);
        margin-bottom: var(--spacing-5);
      }
      .button-area {
        margin-left: var(--spacing-2);
        margin-right: var(--spacing-2);
      }

      /* ... */
    </style>
  </body>
</html>
```

::: tip
余白をつけるときには、対象の要素をdiv要素で囲み、囲んだdiv要素に `margin` を設定する、と説明しましたが、実はdiv要素で囲むことは必須ではありません。

たとえば今回の場合、`class="image"`, `class="heading"`, `class="body"`, `class="button"` にそれぞれ `margin` を指定しても同じ見た目になります。

周辺のパーツとの関係性や現場のコーディング規約をもとに最適な実装方法を探す必要がありますが、余白の情報はパーツ自体には持たせない方が便利なことが多いです。
:::

<a href="../../samples/styling-content-spacing.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [margin - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/margin)
  :::
