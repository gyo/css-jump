# 見出しを作る

## 作るものを確認する

今回作るのはこの部分です。

<img :src="$withBase('/styling-content-heading-goal.png')" alt="今回作る部分" width="375" style="border: 1px solid #eaecef">

## HTMLを書く

`css-jump` ディレクトリの中に `html` ディレクトリを作成し、その中に `styling-content.html` を作成してください。

作成したHTMLには次のように書いておきます。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div>ポラーノの広場</div>

    <style></style>
  </body>
</html>
```

HTMLを書いたらLive Serverを起動し、`http://127.0.0.1:5500/html/styling-content.html` を開いてください。

これから `<div>ポラーノの広場</div>` の部分を、見出しの見た目にしていきます。

## CSSを書くための準備をする

まずは、見た目をつける要素に名前をつけます。今回は見出しを作るので、`heading` という名前をつけることにしましょう。

```html{6}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <style></style>
  </body>
</html>
```

続いて、CSSを用意していきます。先ほど `heading` と名付けたので、`<style>` と `</style>` の間に `heading` のための見た目を定義する場所を作ります。

```html{9-10}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <style>
      .heading {
      }
    </style>
  </body>
</html>
```

## CSSを書く

いよいよ、見た目を定義していきます。

今回の見出しの見た目を作るには、次のような定義が必要です。

- 「フォント」を「明朝体」にする
- 「文字の太さ」を「太い」にする
- 「文字のサイズ」を「通常の文字の2.666倍」にする
- 「行間」を「文字のサイズの1.2倍」にする
- 「文字の色」を「hsl(244, 16%, 18%)（やや薄い黒）」にする

それぞれCSSでは次のように表現します。

- `font-family: serif;`
- `font-weight: bold;`
- `font-size: 2.666rem;`
- `line-height: 1.2;`
- `color: hsl(244, 16%, 18%);`

::: tip
CSSでは `line-height` を使って行間を指定しますが、この行間は文字の上下に2分の1ずつ設定されます。

たとえば `font-size: 20px;`, `line-height: 40px;` のとき、文字の上下に10pxずつ行間が設定されることになります。

<img :src="$withBase('/styling-content-heading-line-height.png')" alt="line-height が文字の上下に設定されるイメージ" width="375">
:::

実際にCSSとして書いてみましょう。

```html{10-14}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
    </style>
  </body>
</html>
```

`http://127.0.0.1:5500/html/styling-content.html` を確認すると、作りたい見た目になっていることを確認できます。

::: tip
見出しのような「折り返しを想定しない」要素であっても、行間について考えなくてよいわけではありません。

実装時には折り返しが発生しなくても、ユーザーの環境でもそうとは限りません。

スマホのような幅の狭い端末で閲覧している可能性もあれば、PCでウィンドウを細長くして閲覧しているかもしれません。コンテンツを拡大して表示しているユーザーもいるかもしれません。こういった状況では、実装者が想定していなくても折り返しが発生することがあるでしょう。
:::

<a href="../../samples/styling-content-heading.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [font-family - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/font-family)
- [font-weight - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/font-weight)
- [font-size - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/font-size)
- [line-height - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/line-height)
- [color - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/color)
  :::
