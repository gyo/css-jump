# 本文を作る

## 作るものを確認する

今回作るのはこの部分です。

<img :src="$withBase('/styling-content-body-goal.png')" alt="今回作る部分" width="375" style="border: 1px solid #eaecef">

## HTMLを書く

まずは、今回作るHTML要素を用意します。

```html{8-10}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div>
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
    </style>
  </body>
</html>
```

## CSSを書くための準備をする

次に、この要素に名前をつけます。今回は本文を作るので、`body` という名前をつけることにしましょう。

```html{8}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
    </style>
  </body>
</html>
```

続いて、CSSを用意しましょう。

```html{20-21}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
      }
    </style>
  </body>
</html>
```

## CSSを書く

最後に見た目を定義していきます。

今回の本文の見た目を作るには、次のような定義が必要です。

- 「フォント」を「ゴシック」にする
- 「文字の太さ」を「普通」にする
- 「文字のサイズ」を「通常の文字と同じ」にする
- 「行間」を「文字のサイズの1.5倍」にする
- 「文字の色」を「hsl(244, 16%, 18%)（やや薄い黒）」にする

それぞれCSSでは次のように表現します。

- `font-family: sans-serif;`
- `font-weight: normal;`
- `font-size: 1rem;`
- `line-height: 1.5;`
- `color: hsl(244, 16%, 18%);`

実際にCSSとして書くと次のようになります。

```html{21-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
    </style>
  </body>
</html>
```

<a href="../../samples/styling-content-body.html" target="_blank">ここまでの成果物</a>

::: tip
実は次の3つは指定しなくても同じ見た目になります。

- `font-family: sans-serif;`
- `font-weight: normal;`
- `font-size: 1rem;`

これはそれぞれのプロパティには初期値があるのが理由です。たとえば `font-weight` には `normal` という初期値が設定されているため、明示的に宣言しないと `font-weight: normal;` の見た目になります。

初期値は仕様で決まっていたりブラウザごとに決まっていたりするので、最初から全てを覚える必要はありません。実際にブラウザで見た目を確認しながらやっているとそのうち自然と覚えられるでしょう。
:::
