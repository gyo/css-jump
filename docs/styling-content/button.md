# ボタンを作る

## 作るものを確認する

今回作るのはこの部分です。

<img :src="$withBase('/styling-content-button-goal.png')" alt="今回作る部分" width="375" style="border: 1px solid #eaecef">

## HTMLを書く

まずはボタンのHTMLを作ります。ボタンは中身に合った幅になってほしいので、div要素ではなくspan要素を使うことにしましょう。

また、CSSを書くための準備も一緒にやってしまいましょう。span要素に `button` という名前をつけ、CSSを書く場所を用意します。

```html{12,29-30}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <span class="button">ボタン</span>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
      }
    </style>
  </body>
</html>
```

## CSSを書く

見出しや本文と同様に、次のような定義が必要なことが想像できます。

- 「フォント」を「ゴシック」にする
- 「文字の太さ」を「太い」にする
- 「文字のサイズ」を「通常の文字と同じ」にする
- 「行間」を「文字のサイズの1倍」にする
- 「文字の色」を「hsl(244, 16%, 98%)（やや暗い白）」にする

また、それ以外にも次のようなものが必要そうです。

- 「背景色」を「hsl(290, 18%, 37%)（薄い臙脂色）」にする
- 「内側の余白」を「上下は1文字分、左右は2文字分」にする
- 「角丸のサイズ」を「0.25文字分」にする

CSSでは次のように表現します。

- `background: hsl(290, 18%, 37%);`
- `padding: 1em 2em;`
- `border-radius: 0.25em;`

```html{30-37}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <span class="button">ボタン</span>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
        padding: 1em 2em;
        border-radius: 0.25em;
        background: hsl(290, 18%, 37%);
        font-family: sans-serif;
        font-weight: bold;
        font-size: 1rem;
        line-height: 1;
        color: hsl(244, 16%, 98%);
      }
    </style>
  </body>
</html>
```

::: tip
`padding` は `padding: 1em 2em;` のように2つの値を書くと、上下に `1em`、左右に `2em` の余白を設定できますが、実は値の個数でそれぞれ設定できる余白の場所が変わります。

- `padding: 1em;` で「上下左右」
- `padding: 1em 1em;` で「上下」「左右」
- `padding: 1em 1em 1em;` で「上」「左右」「下」
- `padding: 1em 1em 1em 1em;` で「上」「右」「下」「左」

たとえば、`padding: 1em 2em;` と `padding: 1em 2em 1em;` と `padding: 1em 2em 1em 2em;` はどれも同じ意味になります。

[padding - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/padding)
:::

## CSSを修正する

ここまでの結果は、次のようになっているはずです。

<img :src="$withBase('/styling-content-button-failure-span.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

一見よさそうですが、ボタンが本文に重なって表示されてしまっています。

これはボタンをspan要素で実装しているためです。span要素には「自身の高さを考慮しない」という性質があるため、上下にある要素と重なってしまうことがあります。

では、span要素ではなくdiv要素を使うとどうなるでしょう。

```html{12}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <div class="button">ボタン</div>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
        padding: 1em 2em;
        border-radius: 0.25em;
        background: hsl(290, 18%, 37%);
        font-family: sans-serif;
        font-weight: bold;
        font-size: 1rem;
        line-height: 1;
        color: hsl(244, 16%, 98%);
      }
    </style>
  </body>
</html>
```

<img :src="$withBase('/styling-content-button-failure-div.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

本文と重なってしまう事態は避けられましたが、今度は横幅がいっぱいに広がってしまっています。

こういった状況で役に立つのが `display: inline-block;` という指定です。さっそく書き足してみましょう。

```html{12,30}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="heading">ポラーノの広場</div>

    <div class="body">
      あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
    </div>

    <span class="button">ボタン</span>

    <style>
      .heading {
        font-family: serif;
        font-weight: bold;
        font-size: 2.666rem;
        line-height: 1.2;
        color: hsl(244, 16%, 18%);
      }
      .body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 1rem;
        line-height: 1.5;
        color: hsl(244, 16%, 18%);
      }
      .button {
        display: inline-block;
        padding: 1em 2em;
        border-radius: 0.25em;
        background: hsl(290, 18%, 37%);
        font-family: sans-serif;
        font-weight: bold;
        font-size: 1rem;
        line-height: 1;
        color: hsl(244, 16%, 98%);
      }
    </style>
  </body>
</html>
```

<img :src="$withBase('/styling-content-button-success.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

本文と重なることもなく、横幅も中身に合った大きさになっています。

::: tip
これまで「span要素は横に並び、div要素は縦に並ぶ」という説明をしてきましたが、実はこれは正確ではありません。

実はこの「横に並ぶ」「縦に並ぶ」は、`display` というプロパティの値で決まっています。`display: inline;` を指定した要素は横に並び、`display: block;` を指定した要素は縦に並びます。

そして、span要素は初期値で `display: inline;` を持っていて、div要素は初期値で `display: block;` を持っているため、それぞれ横と縦に並びます。

[ボックスモデル - ウェブ開発を学ぶ | MDN](https://developer.mozilla.org/ja/docs/Learn/CSS/Building_blocks/The_box_model)
:::

<a href="../../samples/styling-content-button.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。`display` は値が膨大なので、現段階で細部まで読み込む必要はありません。

- [background - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/background)
- [padding - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/padding)
- [border-radius - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/border-radius)
- [display - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/display)
  :::
