# クリックしたときのスタイルをつくる

`<div class="information">` をクリックしたときに、省略しているテキストをすべて表示するようなインタラクションを実装しましょう。

## 現在のCSSを確認する

普段省略しているテキストを表示するには、省略のためのCSSを上書きするようなCSSを用意することから始めます。

まず、テキストの省略に関する記述を確認しておきましょう。`display: -webkit-box;` とそれに関連する記述は以下の部分です。これらは `display: block;` で上書きすることでテキストを全文表示できます。

```css{2,8-10,14,20-22,30}
.information__heading {
  display: -webkit-box;
  font-family: serif;
  font-weight: bold;
  font-size: var(--font-size-large-1);
  line-height: var(--line-height-small);
  color: var(--color-black);
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 1;
  overflow: hidden;
}

.information__body {
  display: -webkit-box;
  font-family: sans-serif;
  font-weight: normal;
  font-size: var(--font-size-small-2);
  line-height: var(--line-height-base);
  color: var(--color-black);
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  overflow: hidden;
}

@media screen and (min-width: 768px) {
  .information__body {
    height: calc(
      var(--font-size-small-2) * var(--line-height-base) * 3
    ); /* .information__body height */
    -webkit-line-clamp: 3;
  }
}
```

次に、テキストの省略と合わせて高さを制限しているところを確認しておきましょう。これらは `height: auto;` で上書きすることでテキストを全文表示できます。

```css{3-6,15-17}
.information {
  display: flex;
  height: calc(
    var(--spacing-1) * 2 + var(--font-size-large-1) * var(--line-height-small) +
      var(--spacing-1) + var(--font-size-small-2) * var(--line-height-base) * 2
  ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
  background: var(--color-gray-light);
  outline: var(--spacing-1) solid transparent;
  transition: background var(--duration-short) var(--easing-standard), outline
      var(--duration-short) var(--easing-standard);
}

@media screen and (min-width: 768px) {
  .information__body {
    height: calc(
      var(--font-size-small-2) * var(--line-height-base) * 3
    ); /* .information__body height */
    -webkit-line-clamp: 3;
  }
}
```

## 上書き用のCSSを作る

上書きしたい要素のclass属性に半角スペース区切りで追記します。ここで追記したclass名に上書きのためのスタイルを適用することになります。

表示の確認ができれば十分なので、全ての `<div class="information">` にではなく3つ目の要素にのみclass名を追記しています。

```html{14,30,35}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-area">
        <div class="information-list">
          ...

          <div class="information-list__item">
            <div class="information information--open">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample3.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample3-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading information__heading--open">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body information__body--open">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                  </div>
                </div>
              </div>
            </div>
          </div>

          ...
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

先ほど追記したclass名に、テキストをすべて表示するためのスタイルを作成します。このとき、上書きするスタイルは上書きされるスタイルよりも後ろに書く必要があることに注意してください。

::: tip
上書きするスタイルを後ろに書かないといけないのは、CSSに「最後に宣言されたものが優先される」というルールがあるためです。

詳しくは [詳細度 - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/Specificity) を参照してください。
:::

```html{31-33,49-51,65-67,81-83}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
        outline: var(--spacing-1) solid transparent;
        transition: background var(--duration-short) var(--easing-standard), outline
            var(--duration-short) var(--easing-standard);
      }

      .information:hover {
        background: var(--color-gray);
        outline: var(--spacing-1) solid var(--color-gray);
        cursor: pointer;
      }

      .information--open {
        height: auto;
      }

      /* ... */

      .information__heading {
        display: -webkit-box;
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
      }

      .information__heading--open {
        display: block;
      }

      .information__body {
        display: -webkit-box;
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
      }

      .information__body--open {
        display: block;
      }

      /* ... */

      @media screen and (min-width: 768px) {
        /* ... */

        .information__body {
          height: calc(
            var(--font-size-small-2) * var(--line-height-base) * 3
          ); /* .information__body height */
          -webkit-line-clamp: 3;
        }

        .information__body--open {
          height: auto;
        }
      }

      /* ... */
    </style>
  </body>
</html>
```

## クリックしたときにclass名を設定する

確認用に振っていたclass名を削除し、ユーザーが `<div class="information">` をクリックしたときにclass名を追記するような処理を記述します。

ユーザーのクリックによってclass名を追加するにはJavaScriptを使います。ここではコピペで大丈夫です。

```html{14,30,35,53-70}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="information-area">
        <div class="information-list">
          ...

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample3.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample3-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                  </div>
                </div>
              </div>
            </div>
          </div>

          ...
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>

    <script>
      const informationElements = document.querySelectorAll(".information");
      informationElements.forEach((informationElement) => {
        informationElement.addEventListener("click", () => {
          informationElement.classList.toggle("information--open");

          const headingElement = informationElement.querySelector(
            ".information__heading"
          );
          headingElement.classList.toggle("information__heading--open");

          const bodyElement = informationElement.querySelector(
            ".information__body"
          );
          bodyElement.classList.toggle("information__body--open");
        });
      });
    </script>
  </body>
</html>
```

## クリック可能なことを表現する

`<div class="information">` にマウスカーソルが入ったときにカーソルの形を変更しましょう。その要素がクリック可能なことをユーザーに伝える効果があります。

```html{14}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information:hover {
        background: var(--color-gray);
        outline: var(--spacing-1) solid var(--color-gray);
        cursor: pointer;
      }

      /* ... */
    </style>

    ...
  </body>
</html>
```

## 要素の最小の高さを固定する

ここまでの実装では、テキストが短い要素をクリックした際に、もともと持っていた高さよりも低くなってしまっています。

それを防ぐために、`min-height` を指定しておきましょう。

```html{13-17,27-29}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information--open {
        height: auto;
        min-height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information height */
      }

      /* ... */

      @media screen and (min-width: 768px) {
        /* ... */

        .information__body--open {
          height: auto;
          min-height: calc(
            var(--font-size-small-2) * var(--line-height-base) * 3
          ); /* .information__body height */
        }
      }

      /* ... */
    </style>
  </body>
</html>
```

<a href="../../samples/interactive-style-click.html" target="_blank">ここまでの成果物</a>

::: tip
上書きするclass名が、`{もとのclass名}--open` となっています。

これもBEMの命名規則のひとつで、何かを上書きするようなclass名はこのような規則で命名します。
:::
