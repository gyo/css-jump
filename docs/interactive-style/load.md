# ページを表示したときのスタイルをつくる

ページを表示したときに要素がフェードインしてくるようなアニメーションを実装してみましょう。

`:hover` のような形でページを表示したときのアニメーションを実現することはできません。JavaScriptと合わせれば実現は可能ですが、ここでは、`animation` を使った方法で実装することにします。

`animation` は、実際の変化を定義する `@keyframes` と合わせて使います。そして `@keyframes` には、変化前のスタイルである `from` と変化後のスタイルである `to` を定義します。

## @keyframesを定義する

たとえばフェードインの場合、不透明度を0%から100%に変化させることで実現します。不透明度は `opacity` で指定します。0%は `0` 100%は `1` と表現します。

```html{11-19}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      @keyframes fade-in {
        from {
          opacity: 0;
        }

        to {
          opacity: 1;
        }
      }
    </style>

    ...
  </body>
</html>
```

## animationを適用する

`@keyframes` で定義した変化を要素に適用するには、`animation` を使い、`animation: 適用したい@keyframesの名前 変化にかかる時間 変化のしかた;` のように書きます。

```html{15-17}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .navigation__item {
        padding: var(--spacing-2);
      }

      .information-list {
        animation: fade-in var(--duration-long) var(--easing-accelerated);
      }

      .information-list__item {
        margin-bottom: var(--spacing-2);
      }

      /* ... */
    </style>

    ...
  </body>
</html>
```

これで、ページを読み込んだときに `<div class="information-list">` がフェードインするようなアニメーションを実装できました。ページを再読み込みしてアニメーションを確認してみてください。

<a href="../../samples/interactive-style-goal.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

[opacity - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/opacity)
[@keyframes - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/@keyframes)
[animation - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/animation)
:::
