# フォーカスしたときのスタイルをつくる

マウスカーソルが合ったときに見た目を変化させてみましょう。

今回は、`<div class="information">` にマウスカーソルを合わせたときに、背景色が濃くなるようにしてみましょう。

現在の見た目は次のようになっています。

<img :src="$withBase('/interactive-style-focus-1.png')" alt="ホバーしていないときのイメージ" width="375" style="border: 1px solid #eaecef">

これを、マウスカーソルを合わせたときに次のような見た目になるようにします。

<img :src="$withBase('/interactive-style-focus-2.png')" alt="ホバーしているときのイメージ" width="375" style="border: 1px solid #eaecef">

## マウスカーソルが合ったときの見た目を実装する

`:hover` を使い、`セレクタ:hover` のように書くことで、マウスカーソルが合ったときの見た目を実装できます。

```html{23-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
      }

      .information:hover {
        background: var(--color-gray);
      }

      /* ... */
    </style>
  </body>
</html>
```

これで、マウスカーソルを合わせると色が変化するようになりました。

## なめらかに変化させる

続いて、色がなめらかに変化するようにしましょう。なめらかに変化させるには、`transition` を使い、`transition: 変化させたいプロパティ 変化にかかる時間 変化のしかた;` のように書きます。

たとえば

- 背景色を変化させる
- 0.125秒かけて変化させる
- 滑らかに始まり滑らかに終わるように変化させる

という場合には、`transition: background 0.125s cubic-bezier(0.4, 0, 0.2, 1);` のように記述します。

変化にかかる時間や変化の仕方は、カスタムプロパティに定義しておくとよいでしょう。

```html{12-18,33}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      :root {
        /* ... */

        --easing-standard: cubic-bezier(0.4, 0, 0.2, 1);
        --easing-decelerated: cubic-bezier(0, 0, 0.2, 1);
        --easing-accelerated: cubic-bezier(0.4, 0, 1, 1);

        --duration-short: 0.125s;
        --duration-medium: 0.25s;
        --duration-long: 0.5s;
      }

      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
        transition: background var(--duration-short) var(--easing-standard);
      }

      .information:hover {
        background: var(--color-gray);
      }

      /* ... */
    </style>
  </body>
</html>
```

::: tip
`cubic-bezier` をつかったタイミング関数の詳細については、[\<timing-function\> - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/timing-function) などを参照してください。

また、タイミング関数は `cubic-bezier` だけでなく、`ease` や `linear` といったキーワードを使った指定も可能です。詳しくは [transition-timing-function - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/transition-timing-function) を参照してください。
:::

## 複数のプロパティをなめらかに変化させる

画像も含む領域がフォーカスされていることを表現するために、テキストエリアの背景色を変化させるだけでなく、全体を囲むように色を変化させてみましょう。

マウスカーソルを合わせたときに次のような見た目にしてみます。

<img :src="$withBase('/interactive-style-focus-3.png')" alt="ホバーしているときのイメージ" width="375" style="border: 1px solid #eaecef">

ここでは、`<div class="information">` に `outline` を設定して表現することにします。

```html{26}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
        transition: background var(--duration-short) var(--easing-standard);
      }

      .information:hover {
        background: var(--color-gray);
        outline: var(--spacing-1) solid var(--color-gray);
      }

      /* ... */
    </style>
  </body>
</html>
```

::: tip
境界線を描画するのに `border` ではなく `outline` を使いました。`border` と `outline` には次のような違いがあります。

- `border` は要素のサイズに含まれるが、`outline` は要素のサイズに含まれない
- `outline` は長方形とは限らない

これらの特徴は制御が難しいため、基本的には `outline` よりも `border` をメインで使い、どうしても事情があるときに `outline` を使うようにするとよいでしょう。
今回のデザインも、HTMLの構造や既存のCSSを修正すれば `border` を使って実現することは可能です。考えてみてください。
:::

これだけでは、`background` が0.125秒かけて変化しているのに対し、`outline` は瞬時に変化するので、変化に差が出てしまいます。そこで、`outline`にも `background` と同様の `transition` 設定を加筆しましょう。複数プロパティの`transition` は、`,` 区切りで記述します。

```html{21-22}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
        transition: background var(--duration-short) var(--easing-standard), outline
            var(--duration-short) var(--easing-standard);
      }

      .information:hover {
        background: var(--color-gray);
        outline: var(--spacing-1) solid var(--color-gray);
      }

      /* ... */
    </style>
  </body>
</html>
```

ここまでで動作を確認すると、違和感に気づくと思います。`outline` の幅が不自然に変化してしまっていたり、色が予期しない色から変化してしまっていたりします。

これはマウスカーソルが合っていないときの `.information` の `outline` が初期値のままなのが原因なので、色以外が変化しないように指定しておきます。もとの色としては、`transparent`（透明）を指定しておきましょう。

```html{21}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
        outline: var(--spacing-1) solid transparent;
        transition: background var(--duration-short) var(--easing-standard), outline
            var(--duration-short) var(--easing-standard);
      }

      .information:hover {
        background: var(--color-gray);
        outline: var(--spacing-1) solid var(--color-gray);
      }

      /* ... */
    </style>
  </body>
</html>
```

::: tip
マウスカーソルが合ったときに見た目を変化させるのに `:hover` を使いましたが、似たような疑似クラスに `:focus`, `:active` などがあります。それぞれ次のような特徴があります。

- `:hover` は、マウスカーソルを要素の上に合わせたとき
- `:focus` は、要素をクリック、タップしたり、Tabキーで選択したとき
- `:focus-within` は、自身もしくは子孫が `:focus` を満たすとき
- `:active` は、クリック中、タップ中のとき

他にも、訪問していないリンクや訪問済みリンクなどを識別する `:link`, `:visited` などがあります。
:::

<a href="../../samples/interactive-style-focus.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場した疑似クラス、CSSプロパティは以下のとおりです。

- [:hover - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/:hover)
- [transition - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/transition)
- [outline - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/outline)
  :::
