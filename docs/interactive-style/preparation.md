# ページ制作のための準備をする

## 元となるHTMLを用意する

`css-jump/html` ディレクトリの中に `interactive-style.html` を作成してください。中身は `responsive-layout.html` でこれまで作ってきたものと同じものを使います。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div class="navigation">
        <div class="navigation__left">
          <div class="navigation__item">Page 1</div>
          <div class="navigation__item">Page 2</div>
        </div>
        <div class="navigation__right">
          <div class="navigation__item">Menu</div>
        </div>
      </div>
    </div>

    <div class="main">
      <div class="information-area">
        <div class="information-list">
          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample1.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample1-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample2.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample2-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample3.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample3-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample4.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample4-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample5.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample5-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample6.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample6-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    あのイーハトーヴォのすきとおった風
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample7.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample7-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="information-list__item">
            <div class="information">
              <div class="information__image-area">
                <picture>
                  <source
                    srcset="../static/sample8.jpg"
                    media="(min-width: 768px)"
                  />
                  <img
                    class="information__image"
                    src="../static/sample8-half.jpg"
                    alt="サンプル"
                  />
                </picture>
              </div>
              <div class="information__text-area">
                <div class="information__heading-area">
                  <div class="information__heading">
                    ポラーノの広場
                  </div>
                </div>
                <div class="information__body-area">
                  <div class="information__body">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      Footer
    </div>

    <style>
      :root {
        --color-black: hsl(244, 16%, 18%);
        --color-white: hsl(244, 16%, 98%);
        --color-gray: hsl(244, 16%, 81%);
        --color-gray-light: hsl(244, 16%, 95%);
        --color-theme: hsl(217, 18%, 28%);
        --color-theme-dimmed: hsl(211, 21%, 41%);
        --color-theme-anti: hsl(244, 16%, 98%);
        --color-accent: hsl(290, 18%, 37%);
        --color-accent-anti: hsl(244, 16%, 98%);
        --color-backdrop: hsla(244, 16%, 18%, 0.5);

        --font-size-small-2: calc(1rem * 8 / 10);
        --font-size-small-1: calc(1rem * 8 / 9);
        --font-size-base: calc(1rem * 8 / 8);
        --font-size-large-1: calc(1rem * 8 / 7);
        --font-size-large-2: calc(1rem * 8 / 6);
        --font-size-large-3: calc(1rem * 8 / 5);
        --font-size-large-4: calc(1rem * 8 / 4);
        --font-size-large-5: calc(1rem * 8 / 3);

        --line-height-small: 1.2;
        --line-height-base: 1.5;

        --spacing-unit: 0.5rem;
        --spacing-1: calc(0.5rem * 1);
        --spacing-2: calc(0.5rem * 2);
        --spacing-3: calc(0.5rem * 3);
        --spacing-5: calc(0.5rem * 5);
        --spacing-8: calc(0.5rem * 8);
      }

      * {
        box-sizing: border-box;
      }

      html,
      body {
        margin: 0;
      }

      img {
        vertical-align: middle;
      }

      .header {
        background: var(--color-theme);
        font-size: var(--font-size-large-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      .main {
        padding-top: var(--spacing-5);
        padding-left: var(--spacing-2);
        padding-right: var(--spacing-2);
      }

      .information-area {
        margin-bottom: var(--spacing-5);
      }

      .footer {
        display: flex;
        justify-content: center;
        padding: var(--spacing-1) var(--spacing-2);
        background: var(--color-theme-dimmed);
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      .navigation {
        display: flex;
      }

      .navigation__left {
        flex-grow: 1;
        display: flex;
      }

      .navigation__right {
        flex-shrink: 0;
      }

      .navigation__item {
        padding: var(--spacing-2);
      }

      .information-list__item {
        margin-bottom: var(--spacing-2);
      }

      .information-list__item:last-child {
        margin-bottom: 0;
      }

      .information {
        display: flex;
        height: calc(
          var(--spacing-1) * 2 + var(--font-size-large-1) * var(
              --line-height-small
            ) + var(--spacing-1) + var(--font-size-small-2) * var(
              --line-height-base
            ) * 2
        ); /* .information__text-area padding * 2 + .information__heading height + .information__heading-area margin-bottom + .information__body height */
        background: var(--color-gray-light);
      }

      .information__image-area {
        flex-shrink: 0;
      }

      .information__image {
        width: calc(var(--spacing-unit) * 12);
        height: 100%;
        object-fit: cover;
      }

      .information__text-area {
        flex-grow: 1;
        padding: var(--spacing-1);
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      .information__heading-area {
        margin-bottom: var(--spacing-1);
      }

      .information__body-area {
      }

      .information__heading {
        display: -webkit-box;
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
      }

      .information__body {
        display: -webkit-box;
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
      }

      @media screen and (min-width: 768px) {
        .information-list {
          display: flex;
          flex-wrap: wrap;
          margin-right: calc(var(--spacing-2) * -1);
          margin-bottom: calc(var(--spacing-2) * -1);
        }

        .information-list__item {
          width: calc(100% / 3 - var(--spacing-2));
          margin-right: var(--spacing-2);
        }

        .information-list__item:last-child {
          margin-bottom: var(--spacing-2);
        }

        .information {
          display: block;
          height: auto;
        }

        .information__image {
          width: 100%;
          height: calc(var(--spacing-unit) * 24);
        }

        .information__text-area {
          display: block;
        }

        .information__body {
          height: calc(
            var(--font-size-small-2) * var(--line-height-base) * 3
          ); /* .information__body height */
          -webkit-line-clamp: 3;
        }
      }

      @media screen and (min-width: 1024px) {
        .information-list__item {
          width: calc(100% / 5 - var(--spacing-2));
        }
      }
    </style>
  </body>
</html>
```
