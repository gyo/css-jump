module.exports = {
  locales: {
    "/": {
      lang: "ja",
    },
  },
  title: "CSS JUMP",
  description: "CSS入門",
  base: "/css-jump/docs/",
  markdown: {
    lineNumbers: true,
  },
  themeConfig: {
    sidebar: [
      {
        title: "はじめに",
        path: "/introduction.md",
      },
      {
        title: "導入準備",
        path: "/getting-started.md",
      },
      {
        title: "HTMLの基礎",
        path: "/html.md",
      },
      {
        title: "CSSの基礎",
        path: "/css-basics.md",
      },
      {
        title: "要素のスタイリング",
        children: [
          {
            title: "要素のスタイリング",
            path: "/styling-content/introduction.md",
          },
          {
            title: "見出しを作る",
            path: "/styling-content/heading.md",
          },
          {
            title: "本文を作る",
            path: "/styling-content/body.md",
          },
          {
            title: "ボタンを作る",
            path: "/styling-content/button.md",
          },
          {
            title: "画像を表示する",
            path: "/styling-content/image.md",
          },
          {
            title: "値を管理する",
            path: "/styling-content/variables.md",
          },
          {
            title: "余白をつける",
            path: "/styling-content/spacing.md",
          },
          {
            title: "画像のサイズとアスペクト比を調整する",
            path: "/styling-content/image-aspect-ratio.md",
          },
        ],
      },
      {
        title: "要素のレイアウト",
        children: [
          {
            title: "要素のレイアウト",
            path: "/layout-content/introduction.md",
          },
          {
            title: "ページ制作のための準備をする",
            path: "/layout-content/preparation.md",
          },
          {
            title: "header, main, footer をつくる",
            path: "/layout-content/layout.md",
          },
          {
            title: "ナビゲーションをつくる",
            path: "/layout-content/navigation.md",
          },
          {
            title: "カード 3 枚のカラムをつくる",
            path: "/layout-content/card-column.md",
          },
          {
            title: "メディアのリストをつくる",
            path: "/layout-content/media-list.md",
          },
          {
            title: "画像のグリッドをつくる",
            path: "/layout-content/image-grid.md",
          },
          {
            title: "ランダムな文字数や画像に対応する",
            path: "/layout-content/randomize.md",
          },
        ],
      },
      {
        title: "レスポンシブなデザイン",
        children: [
          {
            title: "レスポンシブなデザイン",
            path: "/responsive-layout/introduction.md",
          },
          {
            title: "ページ制作のための準備をする",
            path: "/responsive-layout/preparation.md",
          },
          {
            title: "HTMLを組む",
            path: "/responsive-layout/html.md",
          },
          {
            title: "スマホのレイアウトを作る",
            path: "/responsive-layout/mobile-layout.md",
          },
          {
            title: "PCのレイアウトを作る",
            path: "/responsive-layout/desktop-layout.md",
          },
          {
            title: "スマホとPCで画像を切り替える",
            path: "/responsive-layout/switch-image.md",
          },
        ],
      },
      {
        title: "インタラクティブなデザイン",
        children: [
          {
            title: "インタラクティブなデザイン",
            path: "/interactive-style/introduction.md",
          },
          {
            title: "ページ制作のための準備をする",
            path: "/interactive-style/preparation.md",
          },
          {
            title: "フォーカスしたときのスタイルをつくる",
            path: "/interactive-style/focus.md",
          },
          {
            title: "クリックしたときのスタイルをつくる",
            path: "/interactive-style/click.md",
          },
          {
            title: "ページを表示したときのスタイルをつくる",
            path: "/interactive-style/load.md",
          },
        ],
      },
    ],
  },
  plugins: [
    "vuepress-plugin-medium-zoom",
    {
      selector: ".my-wrapper .my-img",
      delay: 1000,
      options: {
        margin: 24,
        background: "#BADA55",
        scrollOffset: 0,
      },
    },
  ],
};
