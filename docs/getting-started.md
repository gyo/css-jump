# 導入準備

## Google Chromeをインストールする

[Google Chrome ウェブブラウザ](https://www.google.com/chrome/)

## VSCode (Visual Studio Code) をインストールする

[Visual Studio Code – コード エディター | Microsoft Azure](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)

### Live Server拡張機能をインストールする

Live Server拡張機能は、自分たちが書いたHTMLやCSSの見た目をブラウザで確認するのに使います。

次のような手順で、VSCodeにインストールします。

アクティビティバーにある、Extensionsタブを開きます。

<img :src="$withBase('/getting-started-vscode-1.png')" alt="VSCode Live Server のインストール手順1" width="375" style="border: 1px solid #eaecef">

検索欄に「live server」と入力します。

<img :src="$withBase('/getting-started-vscode-2.png')" alt="VSCode Live Server のインストール手順2" width="375" style="border: 1px solid #eaecef">

Live Serverがサジェストされるので、「Install」します。

<img :src="$withBase('/getting-started-vscode-3.png')" alt="VSCode Live Server のインストール手順3" width="375" style="border: 1px solid #eaecef">

[Live Server - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

## HTMLを書いてみる

VSCodeを使ってWebページを制作してみましょう。

まずは好きな場所に練習用のディレクトリ（フォルダ）を用意します。

デスクトップなどに `css-jump` というディレクトリを作成したらVSCodeを起動し、アクティビティバーにある、Explorerタブを開きます。

<img :src="$withBase('/getting-started-edit-html-1.png')" alt="HTML を書いてみる 1" width="375" style="border: 1px solid #eaecef">

「Open Folder」から先ほど作成したフォルダを開きます。

<img :src="$withBase('/getting-started-edit-html-2.png')" alt="HTML を書いてみる 2" width="375" style="border: 1px solid #eaecef">

続いて、「New File」をクリックします。

<img :src="$withBase('/getting-started-edit-html-3.png')" alt="HTML を書いてみる 3" width="375" style="border: 1px solid #eaecef">

ファイル名の入力欄が表示されます。

<img :src="$withBase('/getting-started-edit-html-4.png')" alt="HTML を書いてみる 4" width="375" style="border: 1px solid #eaecef">

「index.html」と入力してEnterを押すと、次のような見た目になります。

<img :src="$withBase('/getting-started-edit-html-5.png')" alt="HTML を書いてみる 5" width="375" style="border: 1px solid #eaecef">

簡単なHTMLを書いてみてください。のちの章で説明するので、コピー&ペーストで結構です。

```html
<html>
  <body>
    <h1>Hello World!</h1>
  </body>
</html>
```

<img :src="$withBase('/getting-started-edit-html-6.png')" alt="HTML を書いてみる 6" width="375" style="border: 1px solid #eaecef">

ここで、VSCodeの右下にある「Go Live」というリンクをクリックしてください。

もしリンクが見当たらない場合は、一度VSCodeを閉じて開きなおしてみてください。

<img :src="$withBase('/getting-started-edit-html-7.png')" alt="HTML を書いてみる 7" width="375" style="border: 1px solid #eaecef">

次のような通知が表示され、Chromeが立ち上がります。

<img :src="$withBase('/getting-started-edit-html-8.png')" alt="HTML を書いてみる 8" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/getting-started-edit-html-9.png')" alt="HTML を書いてみる 9" width="375" style="border: 1px solid #eaecef">

これで、`/css-jump/index.html` に書いた内容を `http://127.0.0.1:5500/index.html` で見ることができました。

ためしにHTMLの内容を書き換えると、それがChromeにも反映されることを確認できます。

```html{3}
<html>
  <body>
    <h1>こんにちは</h1>
  </body>
</html>
```

<img :src="$withBase('/getting-started-edit-html-10.png')" alt="HTML を書いてみる 10" width="375" style="border: 1px solid #eaecef">
