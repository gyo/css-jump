# ナビゲーションをつくる

## 構造をイメージする

今回作るのは次のようなナビゲーションです。

<img :src="$withBase('/layout-content-navigation-image.png')" alt="実装するナビゲーションのイメージ" width="375" style="border: 1px solid #eaecef">

まずは、このナビゲーションにどのような四角形があるかイメージしてみてください。いくつかパターンがありますが、たとえば次のような四角形がイメージできるでしょうか。

<img :src="$withBase('/layout-content-navigation-structure.png')" alt="実装するナビゲーションの構造のイメージ" width="375" style="border: 1px solid #eaecef">

構造のみを表示すると次のようになります。

<img :src="$withBase('/layout-content-navigation-skeleton-1.png')" alt="実装するナビゲーションの構造を抽出したイメージ" width="375" style="border: 1px solid #eaecef">

イメージした四角形同士の関係を整理していきましょう。

まずは、一番外側の赤色の四角形とそのひとつ内側のオレンジ色の四角形の関係を明確にしましょう。

<img :src="$withBase('/layout-content-navigation-skeleton-2.png')" alt="実装するナビゲーションの構造の一部を抽出したイメージ" width="375" style="border: 1px solid #eaecef">

まず、赤色の四角形の子要素としてオレンジ色の四角形が2つあることがわかります。
オレンジ色の四角形は左右で長さが違っていますが、右側の四角形はその中身に合った幅になっていて、左側の四角形は残った幅をすべて使うような幅になっています。

次に、左側のオレンジ色の四角形とその内側の緑色の四角形の関係を整理します。

<img :src="$withBase('/layout-content-navigation-skeleton-3.png')" alt="実装するナビゲーションの構造の一部を抽出したイメージ" width="375" style="border: 1px solid #eaecef">

オレンジ色の四角形の子要素として緑色の四角形が2つあり、緑色の四角形は自分たちのコンテンツに合った幅をとって左寄せになっています。

最後に、右側のオレンジ色の四角形とその内側の緑色の四角形の関係を整理します。

<img :src="$withBase('/layout-content-navigation-skeleton-4.png')" alt="実装するナビゲーションの構造の一部を抽出したイメージ" width="375" style="border: 1px solid #eaecef">

ここはオレンジ色の四角形の子要素として緑色の四角形があります。特筆することはないでしょう。

## HTMLを作る

先ほど整理した四角形に合わせて、div要素を組んでいきましょう。

```html{7-15}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div>
        <div>
          <div>Page 1</div>
          <div>Page 2</div>
        </div>
        <div>
          <div>Menu</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

補助的に四角形との対応を書くと次のようになります。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div class="赤">
        <div class="オレンジ">
          <div class="緑">Page 1</div>
          <div class="緑">Page 2</div>
        </div>
        <div class="オレンジ">
          <div class="緑">Menu</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

## CSSを書く

現時点では次のような見た目になっているはずです。

<img :src="$withBase('/layout-content-navigation-styling-1.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

まずは、赤色の四角形とオレンジ色の四角形の見た目上の関係をCSSに書いていきましょう。

要素を横に並べるには、並べたい要素を囲む要素に `display: flex;` を指定します。そして、中身に合わせた幅になってほしい要素には `flex-shrink: 0;`、残リの幅を全て使ってほしい要素には `flex-grow: 1;` を指定します。

```html{7,8,12,22-32}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div class="navigation">
        <div class="navigation__left">
          <div>Page 1</div>
          <div>Page 2</div>
        </div>
        <div class="navigation__right">
          <div>Menu</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .navigation {
        display: flex;
      }

      .navigation__left {
        flex-grow: 1;
      }

      .navigation__right {
        flex-shrink: 0;
      }
    </style>
  </body>
</html>
```

ここまでで、赤色の四角形とオレンジ色の四角形の見た目上の関係が実装されました。「Page 1」「Page 2」と「Menu」が左右に分かれているのが確認できます。

<img :src="$withBase('/layout-content-navigation-styling-2.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

続いて左側のオレンジ色の四角形とその内側の緑色の四角形について、CSSを書いていきましょう。

ここで、緑色の四角形は横並びになっているので、その親要素であるオレンジ色の四角形に `display: flex;` を指定します。緑色の四角形はコンテンツに合った幅で左寄せになっていますが、`display: flex;` はデフォルトでそのような表示になるのでこれ以上指定する必要はありません。

```html{28}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div class="navigation">
        <div class="navigation__left">
          <div>Page 1</div>
          <div>Page 2</div>
        </div>
        <div class="navigation__right">
          <div>Menu</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .navigation {
        display: flex;
      }

      .navigation__left {
        flex-grow: 1;
        display: flex;
      }

      .navigation__right {
        flex-shrink: 0;
      }
    </style>
  </body>
</html>
```

ここまでで、「Page 1」「Page 2」を左寄せの横並びにできました。

<img :src="$withBase('/layout-content-navigation-styling-3.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

最後に、「Page 1」「Page 2」の間の余白などを調整します。
もともとheaderについていた余白を削除し、「Page 1」「Page 2」「Menu」に余白を設定しましょう。

```html{9-10,13,27,49-51}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      <div class="navigation">
        <div class="navigation__left">
          <div class="navigation__item">Page 1</div>
          <div class="navigation__item">Page 2</div>
        </div>
        <div class="navigation__right">
          <div class="navigation__item">Menu</div>
        </div>
      </div>
    </div>

    <div class="main">
      Main
    </div>

    ...
    <style>
      /* ... */

      .header {
        /* padding: var(--spacing-2); を削除 */
        background: var(--color-theme);
        font-size: var(--font-size-large-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      /* ... */

      .navigation {
        display: flex;
      }

      .navigation__left {
        flex-grow: 1;
        display: flex;
      }

      .navigation__right {
        flex-shrink: 0;
      }

      .navigation__item {
        padding: var(--spacing-2);
      }
    </style>
  </body>
</html>
```

これで、めざしていた見た目を作ることが出来ました。

<img :src="$withBase('/layout-content-navigation-styling-4.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

<a href="../../samples/layout-content-navigation.html" target="_blank">ここまでの成果物</a>

## まとめ

- 要素を横並びにしたいときには、それらの要素の親要素に `display: flex;` を指定する
- `display: flex;` は、子要素をコンテンツの幅で左寄せで横並びにする
- 横幅が伸びて欲しい要素には `flex-grow: 1;` を指定する。`flex-grow: 1;` を指定した場合、コンテンツの幅になってほしい要素には `flex-shrink: 0;` を指定する

::: tip
ナビゲーション関連のコンポーネントのclass名が、ある規則でつけられていることに気づいたかもしれません。

あるパーツに「navigation」という名前をつけたのであれば、そのパーツを構成するパーツには `navigation__{構成するパーツ名}` という名前をつけるようにしています。

これはBEMと呼ばれる命名規則で、CSSのメンテナンス性を高める効果があります。
:::

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [flex-grow - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/flex-grow)
- [flex-shrink - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/flex-shrink)
  :::
