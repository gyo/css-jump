# header, main, footerをつくる

## HTMLを作る

まずは作るもののイメージを固めましょう。

Webページにはよくあるパーツがあります。有名なものでは、ロゴやグローバルナビゲーションがあるheader、コピーライトやリンクがあるfooter、ページの主要なコンテンツがあるmainなどがあげられます。

<img :src="$withBase('/layout-content-layout.png')" width="375" alt="header, main, footer のイメージ">

こういった、コンテンツの入れ物となる部分を作っていきます。

header, main, footerの四角形に着目すると、どれもそれぞれ横いっぱいの幅で縦に並んでいます。そのため、[div 要素](/html.md#div要素)を使うことにしましょう。

```html{6-16}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="header">
      Header
    </div>

    <div class="main">
      Main
    </div>

    <div class="footer">
      Footer
    </div>

    <style>
      /* ... */
    </style>
  </body>
</html>
```

## CSSを書く

背景色、フォント、余白といった、基本的なものを定義しましょう。

```html{11-29}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .header {
        padding: var(--spacing-2);
        background: var(--color-theme);
        font-size: var(--font-size-large-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      .main {
        padding: var(--spacing-2);
      }

      .footer {
        padding: var(--spacing-1) var(--spacing-2);
        background: var(--color-theme-dimmed);
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }
    </style>
  </body>
</html>
```

最後に、footerの中身を中央寄せします。

要素を左右の中央に配置するには、`display: flex;` と `justify-content: center;` という指定を使います。

```html{24-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .header {
        padding: var(--spacing-2);
        background: var(--color-theme);
        font-size: var(--font-size-large-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }

      .main {
        padding: var(--spacing-2);
      }

      .footer {
        display: flex;
        justify-content: center;
        padding: var(--spacing-1) var(--spacing-2);
        background: var(--color-theme-dimmed);
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-theme-anti);
      }
    </style>
  </body>
</html>
```

<a href="../../samples/layout-content-layout.html" target="_blank">ここまでの成果物</a>

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [justify-content - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/justify-content)
  :::
