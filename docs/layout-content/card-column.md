# カード3枚のカラムをつくる

## 構造をイメージする

今回作るのは次のようなカラムです。

<img :src="$withBase('/layout-content-card-column-image.png')" alt="実装するカラムのイメージ" width="375" style="border: 1px solid #eaecef">

構成する四角形をイメージしてみてください。次のようなイメージが浮かぶでしょうか。

<img :src="$withBase('/layout-content-card-column-structure.png')" alt="実装するカラムの構造のイメージ" width="375" style="border: 1px solid #eaecef">

構造のみを表示すると次のようになります。

<img :src="$withBase('/layout-content-card-column-skeleton.png')" alt="実装するカラムの構造を抽出したイメージ" width="375" style="border: 1px solid #eaecef">

赤色の四角形の中にオレンジ色の四角形が3つ横並びになっていて、オレンジ色の四角形の幅は赤色の四角形の幅の3分の1になっています。オレンジ色の四角形の中には緑色の四角形が3つ縦に並んでいます。緑色の四角形の高さはまずはコンテンツの高さに合わせておきましょう。

## カードを作る

まずはカードのひとつから作っていきましょう。オレンジ色の四角形ひとつと、その中にある緑色の四角形3つの部分です。

カードに表示するサンプルの画像はこちらです。

<a href="../../static/sample1.jpg" target="_blank">sample1.jpg</a>

画像をダウンロードしたら `css-jump/static` ディレクトリの中に `sample1.jpg` を保存してください。

```html{9-21}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          ポラーノの広場
        </div>

        <div class="card__body-area">
          あのイーハトーヴォのすきとおった風
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

それぞれのCSSを書くと次のようになります。

```html{28-50}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          ポラーノの広場
        </div>

        <div class="card__body-area">
          あのイーハトーヴォのすきとおった風
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .card {
        background: var(--color-gray-light);
      }

      .card__image-area {
        margin-bottom: var(--spacing-1);
      }

      .card__heading-area {
        margin-bottom: var(--spacing-1);
        padding-left: var(--spacing-1);
        padding-right: var(--spacing-1);
      }

      .card__body-area {
        padding-left: var(--spacing-1);
        padding-right: var(--spacing-1);
        padding-bottom: var(--spacing-1);
      }

      .card__image {
        width: 100%;
      }
    </style>
  </body>
</html>
```

カードのレイアウトができたので、それぞれのコンテンツの調整をしていきましょう。
ここでは、タイトルと本文のフォントを調整する必要があるため、それらのパーツに名前をつけ、スタイルを定義しましょう。

```html{15,17,21,23,32-46}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          <div class="card__heading">
            ポラーノの広場
          </div>
        </div>

        <div class="card__body-area">
          <div class="card__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .card__heading {
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
      }

      .card__body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
      }
    </style>
  </body>
</html>
```

## カラムを作る

さきほど作ったカードを3つに増やし、横並びにしていきます。
まずは、先ほどと同じ構造でカードの数を増やしましょう。

```html{27-61}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          <div class="card__heading">
            ポラーノの広場
          </div>
        </div>

        <div class="card__body-area">
          <div class="card__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          <div class="card__heading">
            ポラーノの広場
          </div>
        </div>

        <div class="card__body-area">
          <div class="card__body">
            夏でも底に冷たさをもつ青いそら
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card__image-area">
          <img class="card__image" src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="card__heading-area">
          <div class="card__heading">
            ポラーノの広場
          </div>
        </div>

        <div class="card__body-area">
          <div class="card__body">
            うつくしい森で飾られたモリーオ市
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

現時点では次のように、これらのカードは縦に並んでいます。

<img :src="$withBase('/layout-content-card-column-styling-1.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

これを、赤色の四角形とオレンジ色の四角形の見た目になるようにしていきます。

縦に並んだ要素を横に並べるには、[ナビゲーションをつくる](/layout-content/navigation.md) であったように、並べたい要素を囲む要素に `display: flex;` を指定します。
そのために、カード3つを新しいdiv要素で囲み、`display: flex;` を指定しましょう。

```html{9,21,28-30}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card-column">
        <div class="card">
          ...
        </div>

        <div class="card">
          ...
        </div>

        <div class="card">
          ...
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .card-column {
        display: flex;
      }

      .card {
        background: var(--color-gray-light);
      }

      /* ... */
    </style>
  </body>
</html>
```

それでは、オレンジ色の四角形が赤色の四角形の3分の1の幅になるようにCSSを書きましょう。同時に、これらのオレンジ色の四角形の間の余白も設定していきます。

幅の制限と余白を設けるためにまず、`class="card"` をdiv要素で囲みます。

```html{10,14,16,20,22,26}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card-column">
        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>

        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>

        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

`class="card-column"` と `class="card-column__item"` が赤色の四角形とオレンジ色の四角形にあたります。

幅を3分の1にするには `width: calc(100% / 3);` とすればよかったのですが、今回は幅が3分の1といっても、オレンジ色の四角形同士の間には隙間があります。なので、単純に `width: calc(100% / 3);` とするのではなく、 100% から余白分を引いた残りを3分の1にするように `width: calc((100% - var(--spacing-2) * 2) / 3);` と指定しましょう。

そして `class="card-column__item"` 同士の間に余白を設けるには、`.card-column__item + .card-column__item` に `margin-left` を設定するという書き方をします。この `.A + .B` という書き方は正確には、「Aの直後のB」という意味になります。
たとえば `.card-column__item` に `margin-left` を設定してしまうと1つ目のカードの左側に余計な余白がついてしまいますが、`.card-column__item + .card-column__item` という書き方をすることによって必要なところだけに余白を設定できます。

```html{42-48}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card-column">
        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>

        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>

        <div class="card-column__item">
          <div class="card">
            ...
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .navigation__item {
        padding: var(--spacing-2);
      }

      .card-column {
        display: flex;
      }

      .card-column__item {
        width: calc((100% - var(--spacing-2) * 2) / 3);
      }

      .card-column__item + .card-column__item {
        margin-left: var(--spacing-2);
      }

      .card {
        background: var(--color-gray-light);
      }

      /* ... */
    </style>
  </body>
</html>
```

<a href="../../samples/layout-content-card-column.html" target="_blank">ここまでの成果物</a>

## まとめ

- 「Aの直後のB」にスタイルを当てるときには、`.A + .B` と書く

::: tip
この章で新たに登場したCSSセレクタ、CSSプロパティは以下のとおりです。

- [隣接兄弟結合子 - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/Adjacent_sibling_combinator)
  :::
