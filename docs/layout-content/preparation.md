# ページ制作のための準備をする

## 空のHTMLを用意する

`css-jump/html` ディレクトリの中に `layout-content.html` を作成してください。

```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <style></style>
  </body>
</html>
```

## デザイントークンを定義する

フォントサイズや色など、ページ内で使うデザイントークンを定義します。

`calc()` という表記がありますが、これは `()` の中を計算する、という記述です。
たとえば100% の3分の1のサイズにしたいときに、数値では `33.33333%` のような近似値を書くことしか出来ませんが、`calc()` を使うことで `calc(100% / 3)` といった書き方ができるようになります。

```html{7-37}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <style>
      :root {
        --color-black: hsl(244, 16%, 18%);
        --color-white: hsl(244, 16%, 98%);
        --color-gray: hsl(244, 16%, 81%);
        --color-gray-light: hsl(244, 16%, 95%);
        --color-theme: hsl(217, 18%, 28%);
        --color-theme-dimmed: hsl(211, 21%, 41%);
        --color-theme-anti: hsl(244, 16%, 98%);
        --color-accent: hsl(290, 18%, 37%);
        --color-accent-anti: hsl(244, 16%, 98%);
        --color-backdrop: hsla(244, 16%, 18%, 0.5);

        --font-size-small-2: calc(1rem * 8 / 10);
        --font-size-small-1: calc(1rem * 8 / 9);
        --font-size-base: calc(1rem * 8 / 8);
        --font-size-large-1: calc(1rem * 8 / 7);
        --font-size-large-2: calc(1rem * 8 / 6);
        --font-size-large-3: calc(1rem * 8 / 5);
        --font-size-large-4: calc(1rem * 8 / 4);
        --font-size-large-5: calc(1rem * 8 / 3);

        --line-height-small: 1.2;
        --line-height-base: 1.5;

        --spacing-unit: 0.5rem;
        --spacing-1: calc(0.5rem * 1);
        --spacing-2: calc(0.5rem * 2);
        --spacing-3: calc(0.5rem * 3);
        --spacing-5: calc(0.5rem * 5);
        --spacing-8: calc(0.5rem * 8);
      }
    </style>
  </body>
</html>
```

## あると便利なスタイルをあらかじめ用意しておく

[「全体の余白を消す」](/styling-content/spacing.md#全体の余白を消す) でbody要素の周りの余白を打ち消したように、あらかじめ適用しておくと便利なスタイルがあるのでそれを記述します。

`*` は「すべて」という意味で、すべての要素に `box-sizing: border-box;` と指定しています。これは `width` や `height` といったサイズを直観的に記述するための指定です。

```html{9-20}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <style>
      /* ... */

      * {
        box-sizing: border-box;
      }

      html,
      body {
        margin: 0;
      }

      img {
        vertical-align: middle;
      }
    </style>
  </body>
</html>
```

::: tip
`box-sizing: border-box;` を指定することで、見た目上の要素のサイズと `width` で指定したサイズを揃えられます。

指定しない場合、要素の見た目上の幅は `width` + `padding` + `border` のサイズになります。要素の幅が `width` で指定した値よりも大きくなり、レイアウト時に想定外の挙動に悩まされることが多くなってしまいます。

そのため、box-sizing: border-box; を指定しておくのが一般的です。

[CSS 基本ボックスモデルの紹介 - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model)
:::

::: tip
この章で新たに登場したCSS関数、CSSセレクター、CSSプロパティは以下のとおりです。

- [calc() - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/calc)
- [全称セレクター - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/Universal_selectors)
- [CSS box-sizing - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/box-sizing)
- [vertical-align - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/vertical-align)
  :::
