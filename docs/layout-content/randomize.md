# ランダムな文字数や画像に対応する

## 表示を確認する

Webページは制作時に想定していなかったデータを表示することがあるので、そういった場合にも対応できるようにスタイルを実装しておく必要があります。
まずは、ためしに文字数やテキストをランダムに変更してみましょう。

サンプルの画像はこちらです。

<a href="../../static/sample2.jpg" target="_blank">sample2.jpg</a>, <a href="../../static/sample3.jpg" target="_blank">sample3.jpg</a>, <a href="../../static/sample4.jpg" target="_blank">sample4.jpg</a>, <a href="../../static/sample5.jpg" target="_blank">sample5.jpg</a>, <a href="../../static/sample6.jpg" target="_blank">sample6.jpg</a>, <a href="../../static/sample7.jpg" target="_blank">sample7.jpg</a>, <a href="../../static/sample8.jpg" target="_blank">sample8.jpg</a>, <a href="../../static/sample9.jpg" target="_blank">sample9.jpg</a>

画像をダウンロードしたら `css-jump/static` ディレクトリの中に保存してください。

```html{40,47,64,77,116,124,140,153,164,172,177,204,205,208,217,218,221,230,231,234,235,236,245,258,259,262,271,272,275,284,285,288,289,290,299}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card-column-area">
        <div class="card-column">
          <div class="card-column__item">
            <div class="card">
              <div class="card__image-area">
                <img
                  class="card__image"
                  src="../static/sample1.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="card__heading-area">
                <div class="card__heading">
                  ポラーノの広場
                </div>
              </div>

              <div class="card__body-area">
                <div class="card__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>

          <div class="card-column__item">
            <div class="card">
              <div class="card__image-area">
                <img
                  class="card__image"
                  src="../static/sample2.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="card__heading-area">
                <div class="card__heading">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>

              <div class="card__body-area">
                <div class="card__body">
                  夏でも底に冷たさをもつ青いそら
                </div>
              </div>
            </div>
          </div>

          <div class="card-column__item">
            <div class="card">
              <div class="card__image-area">
                <img
                  class="card__image"
                  src="../static/sample3.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="card__heading-area">
                <div class="card__heading">
                  ポラーノの広場
                </div>
              </div>

              <div class="card__body-area">
                <div class="card__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="media-list-area">
        <div class="media-list">
          <div class="media-list__item">
            <div class="media">
              <div class="media__image-area">
                <img
                  class="media__image"
                  src="../static/sample1.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="media__text-area">
                <div class="media__heading-area">
                  <div class="media__heading">
                    ポラーノの広場
                  </div>
                </div>

                <div class="media__body">
                  あのイーハトーヴォのすきとおった風
                </div>
              </div>
            </div>
          </div>

          <div class="media-list__item">
            <div class="media">
              <div class="media__image-area">
                <img
                  class="media__image"
                  src="../static/sample2.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="media__text-area">
                <div class="media__heading-area">
                  <div class="media__heading">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>

                <div class="media__body">
                  夏でも底に冷たさをもつ青いそら
                </div>
              </div>
            </div>
          </div>

          <div class="media-list__item">
            <div class="media">
              <div class="media__image-area">
                <img
                  class="media__image"
                  src="../static/sample3.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="media__text-area">
                <div class="media__heading-area">
                  <div class="media__heading">
                    ポラーノの広場
                  </div>
                </div>

                <div class="media__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>

          <div class="media-list__item">
            <div class="media">
              <div class="media__image-area">
                <img
                  class="media__image"
                  src="../static/sample4.jpg"
                  alt="サンプル"
                />
              </div>

              <div class="media__text-area">
                <div class="media__heading-area">
                  <div class="media__heading">
                    あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                  </div>
                </div>

                <div class="media__body">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tile-grid-area">
        <div class="tile-grid">
          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample1.jpg"
                alt="サンプル"
              />
              <div class="tile__text-area">
                <div class="tile__text">サンプル</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample2.jpg"
                alt="ポラーノの広場"
              />
              <div class="tile__text-area">
                <div class="tile__text">ポラーノの広場</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample3.jpg"
                alt="あのイーハトーヴォのすきとおった風"
              />
              <div class="tile__text-area">
                <div class="tile__text">あのイーハトーヴォのすきとおった風</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample4.jpg"
                alt="あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。"
              />
              <div class="tile__text-area">
                <div class="tile__text">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample5.jpg"
                alt="サンプル"
              />
              <div class="tile__text-area">
                <div class="tile__text">サンプル</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample6.jpg"
                alt="ポラーノの広場"
              />
              <div class="tile__text-area">
                <div class="tile__text">ポラーノの広場</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample7.jpg"
                alt="あのイーハトーヴォのすきとおった風"
              />
              <div class="tile__text-area">
                <div class="tile__text">あのイーハトーヴォのすきとおった風</div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample8.jpg"
                alt="あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。"
              />
              <div class="tile__text-area">
                <div class="tile__text">
                  あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
                </div>
              </div>
            </div>
          </div>

          <div class="tile-grid__item">
            <div class="tile">
              <img
                class="tile__image"
                src="../static/sample9.jpg"
                alt="サンプル"
              />
              <div class="tile__text-area">
                <div class="tile__text">サンプル</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      Footer
    </div>

    <style>
      /* ... */
    </style>
  </body>
</html>
```

このときの見た目は次のようになっています。

<img :src="$withBase('/layout-content-randomize-styling-1.png')" width="375" style="border: 1px solid #eaecef" alt="文字数やテキストをランダムに変更したイメージ">

このままではいくつもの問題点があることに気づくでしょうか。

カード3枚のカラムやメディアのリストでは、カードごとの高さや画像の占める割合がバラバラになってしまっています。
画像のグリッドにおいては、文字数が多すぎるときに画像がほとんど見えなくなっていたり、ひどい場合には他の画像と重なってしまっています。

## カード3枚のカラムを修正する

たとえば、次のような方針で修正することとします。

- カードの高さをそろえる
- 画像の高さをそろえる
- タイトルを3行以内にトリミングする
- 本文を7行以内にトリミングする

::: tip
実際のWebサイト制作でテキストを省略する場合には、省略されていない全文を確認する手段を別途用意しましょう。

たとえば詳細ページを用意してそこに遷移できるようにしたり、他にもモーダルダイアログや折りたたみ要素を使う方法も考えられます。
:::

カードの高さをそろえるにあたり、注目すべきは次の3点です。

- `.card-column` に指定されている `display: flex;`
- `.card` に指定されている `background: var(--color-gray-light);`
- `<div class="card-column">` と `<div class="card">` の間には `<div class="card-column__item">` が存在する

もともと `display: flex;` によって横並びにされた要素は同じ高さをとるようになっています。
そのため、`<div class="card-column">` の子要素である `<div class="card-column__item">` は同じ高さになっています。
ところが、背景色がついている `<div class="card">` はさらにその内側にあります。要素は通常、親の要素の高さに合わせて伸びるようなことはないので、`<div class="card-column__item">` よりも `<div class="card">` が小さくなってしまうことが起こりえます。

`width: 100%;` と同様に、`%` 単位を使うことで親要素の高さに対する割合を指定できるので、親要素の高さと子要素の高さをそろえるには、子要素に `height: 100%;` を指定します。

```html{12}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .card {
        height: 100%;
        background: var(--color-gray-light);
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-2.png')" width="375" style="border: 1px solid #eaecef" alt="カードの高さをそろえたイメージ">

画像の高さをそろえるには、まずそろえる高さを決めないといけませんが、ここでは固定値でそろえてしまうことにします。
ですが `height` を固定するだけでは画像のアスペクト比が崩れてしまうので、アスペクト比を維持するための `object-fit` も合わせて実装する必要があります。

```html{13,14}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .card__image {
        width: 100%;
        height: calc(var(--spacing-unit) * 10);
        object-fit: cover;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-3.png')" width="375" style="border: 1px solid #eaecef" alt="カードの画像の高さをそろえたイメージ">

テキストを複数行でトリミングするには、少し複雑な指定が必要です。
これを実装するには、トリミングしたい要素、またはその親要素に `overflow: hidden;` を指定し、トリミングしたい要素に `display: -webkit-box;`, `-webkit-box-orient: vertical;`, `-webkit-line-clamp: {表示したい行数};` を指定します。

```html{12,18-20,24,30-32}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .card__heading {
        display: -webkit-box;
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 3;
        overflow: hidden;
      }

      .card__body {
        display: -webkit-box;
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 7;
        overflow: hidden;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-4.png')" width="375" style="border: 1px solid #eaecef" alt="カードのタイトルと本文をトリミングしたイメージ">

これで、文字数や画像サイズが変わっても崩れないデザインを実装できました。
もっと良いデザインを思いついた方もいると思います。ぜひいろいろと試行錯誤をしてみてください。

## メディアのリストを修正する

次のような方針で修正することとします。

- 画像の高さをメディアの高さにそろえる
- タイトルを1行以内にトリミングする
- 本文を3行以内にトリミングする

画像の高さをそろえるにあたり、ポイントは以下の2点です。

- `.media` に指定されている `display: flex;`
- `<div class="media">` と `<img class="media__image">` の間には `<div class="media__image-area">` が存在する

`<div class="media">` に `display: flex;` が指定されているため、`<img class="media__image">` の高さは `<div class="media">` の高さとそろっています。
そのため画像の高さをメディアの高さにそろえるには、カード3枚のカラムを修正したときと同様に `<img class="media__image">` に `height: 100%;` を指定すればよいでしょう。
ただし画像の幅と高さを指定するときには、アスペクト比を維持するための `object-fit: cover;` も忘れずに指定しておきます。

```html{13-14}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .media__image {
        width: calc(var(--spacing-unit) * 12);
        height: 100%;
        object-fit: cover;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-5.png')" width="375" style="border: 1px solid #eaecef" alt="メディアの画像の高さをそろえたイメージ">

タイトルと本文をトリミングするには、カード3枚のカラムを修正したときと同様に実装します。

```html{12,18-20,24,30-32}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .media__heading {
        display: -webkit-box;
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
      }

      .media__body {
        display: -webkit-box;
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 3;
        overflow: hidden;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-6.png')" width="375" style="border: 1px solid #eaecef" alt="メディアのタイトルと本文をトリミングしたイメージ">

## 画像のグリッドを修正する

画像のグリッドも、これまでと同じような方針で修正できそうです。

- 画像のサイズをそろえる
- テキストを1行以内にトリミングする

`.tile-grid` に `display: flex;` が指定されているため、また
画像のサイズをそろえるためには `height: 100%;` を指定したいところですが、今回はそれではうまくいきません。

ためしに `height: 100%;` を指定してみましょう。

```html{13,18-19}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .tile {
        position: relative;
        height: 100%;
      }

      .tile__image {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }

      /* ... */
    </style>
  </body>
</html>
```

次のような見た目になってしまいます。

<img :src="$withBase('/layout-content-randomize-styling-7.png')" width="375" style="border: 1px solid #eaecef" alt="タイルリストに height: 100%; を指定した際のイメージ">

詳細は割愛しますが、`<div class="tile">` の中身が次のような特徴をもつ要素であることが原因です。

- img要素
- `position: absolute;` を指定された要素

こういった場合には、親要素に `height` を指定することで高さを出すことができます。

```html{13}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .tile-grid__item {
        width: calc((100% - var(--spacing-1) * 2) / 3);
        height: calc(var(--spacing-unit) * 12);
        margin-right: var(--spacing-1);
        margin-bottom: var(--spacing-1);
      }

      /* ... */

      .tile {
        position: relative;
        height: 100%;
      }

      .tile__image {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }

      /* ... */
    </style>
  </body>
</html>
```

テキストのトリミングは、これまでと同じ方法で実現可能です。

```html{12,16-18}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <style>
      /* ... */

      .tile__text {
        display: -webkit-box;
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-white);
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-randomize-styling-8.png')" width="375" style="border: 1px solid #eaecef" alt="タイルリストの画像の高さをそろえたイメージ">

<a href="../../samples/layout-content-goal-randomize.html" target="_blank">ここまでの成果物</a>

## まとめ

- `display: flex;` によって横並びにされた要素は同じ高さをとるようになる
- 複数行のテキストのトリミングには、`overflow: hidden;`, `display: -webkit-box;`, `-webkit-box-orient: vertical;`, `-webkit-line-clamp: {表示したい行数};` を使う
- img要素や、`position: absolute;` を指定された要素があるときには高さの指定方法に注意する

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [height - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/height)
- [overflow - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/overflow)
- [box-orient - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/box-orient)
- [-webkit-line-clamp - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/-webkit-line-clamp)
  :::
