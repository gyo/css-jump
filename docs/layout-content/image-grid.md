# 画像のグリッドをつくる

## 構造をイメージする

今回作るのは次のようなグリッドです。

<img :src="$withBase('/layout-content-image-grid-image.png')" alt="実装するグリッドのイメージ" width="375" style="border: 1px solid #eaecef">

これまでと同様に、レイアウトを構成する四角形をイメージしてみてください。

<img :src="$withBase('/layout-content-image-grid-structure.png')" alt="実装するグリッドの構造のイメージ" width="375" style="border: 1px solid #eaecef">

構造のみを表示すると次のようになります。

<img :src="$withBase('/layout-content-image-grid-skeleton-1.png')" alt="実装するグリッドの構造を抽出したイメージ1" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-image-grid-skeleton-2.png')" alt="実装するグリッドの構造を抽出したイメージ2" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-image-grid-skeleton-3.png')" alt="実装するグリッドの構造を抽出したイメージ3" width="375" style="border: 1px solid #eaecef">

赤色の四角形の中に、 9個のオレンジ色の四角形が縦横に、左上から右方向に3つずつ折り返して並んでいます。
緑色の四角形と水色の四角形の関係は特殊で、緑色の四角形に重なるように水色の四角形が配置されています。

## 画像にテキストを重ねる

まずは緑色の四角形と水色の四角形の部分を作っていきます。

まずは、HTMLを用意しましょう。
見やすくするために、画像とテキストのCSSも書いておきましょう。

```html{11-16,23-39}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .tile {
      }

      .tile__image {
        width: 100%;
      }

      .tile__text-area {
        padding: var(--spacing-1);
        background: var(--color-backdrop);
      }

      .tile__text {
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-white);
      }
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-image-grid-styling-1.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

ある要素を他の要素に重ねるには、まずは重ねられる要素に `position: relative;`、重ねる要素に `position: absolute;` を指定するところから始めます。

```html{24,32}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .tile {
        position: relative;
      }

      .tile__image {
        width: 100%;
      }

      .tile__text-area {
        position: absolute;
        padding: var(--spacing-1);
        background: var(--color-backdrop);
      }

      .tile__text {
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-white);
      }
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-image-grid-styling-2.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

見た通り、`position: absolute;` と指定された要素は、たとえそれがdiv要素であってもコンテンツに合わせた幅をとるようになります。そのため、明示的に `width: 100%` を指定する必要があります。

また、`position: absolute;` だけでは任意の位置に表示できていません。これを任意の位置に表示するには、`top`, `left`, `right`, `bottom` という指定を使います。
たとえば、`position: absolute;` を指定した要素に `top: 100px;` を指定すると、`position: relative;` を指定した要素の上端から100px下の位置に `position: absolute;` を指定した要素の上端が来るようになります。
今回は、緑色の四角形と水色の四角形の下端、右端、左端をそれぞれそろえるような位置に表示したいので、`bottom: 0;`, `right: 0;`, `left: 0;` を指定しましょう。

```html{33-36}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .tile {
        position: relative;
      }

      .tile__image {
        width: 100%;
      }

      .tile__text-area {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        padding: var(--spacing-1);
        background: var(--color-backdrop);
      }

      .tile__text {
        font-size: var(--font-size-small-1);
        line-height: 1;
        color: var(--color-white);
      }
    </style>
  </body>
</html>
```

これで、画像とテキストを重ねて表示できました。

<img :src="$withBase('/layout-content-image-grid-styling-3.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

## 画像のグリッドを作る

つぎに、画像を縦横に配置していきます。
まずは先ほど作った `class="tile"` を個数分用意しておきましょう。

```html{18-72}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>

      <div class="tile">
        <img class="tile__image" src="../static/sample1.jpg" alt="サンプル" />
        <div class="tile__text-area">
          <div class="tile__text">サンプル</div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

続いて、画像がそれぞれウィンドウの幅の3分の1になるようにCSSを書きましょう。

画像を縦横に配置するには、`display: flex;` に加えて `flex-wrap: wrap;` という指定をします。
幅を3分の1にするには、[カラムを作る](/layout-content/card-column.md#カラムを作る) でやったように、 `width: calc(100% / 3);` のような記述を使えばよさそうです。

```html{11,12,16,18,22,24,28,30,34,36,40,42,46,48,52,54,58,60,64,65,72-79}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile-grid">
        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .tile-grid {
        display: flex;
        flex-wrap: wrap;
      }

      .tile-grid__item {
        width: calc(100% / 3);
      }

      .tile {
        position: relative;
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-image-grid-styling-4.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

<!-- <img :src="$withBase('/layout-content-image-grid-styling-1.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef"> -->

続けてグリッドの余白を設定していきます。

`class="tile-grid__item"` に `margin-right` と `margin-bottom` を指定すればよさそうですが、この場合も余計な余白がついてしまいます。
3, 6, 9個目の画像の `margin-right` と、7, 8, 9個目の画像の `margin-bottom` は不要ですね。
これらの不要な余白は削除する必要があります。

[メディアのリストを作る](layout-content/media-list.html#メディアのリストを作る) 紹介した `:last-child` と似たような指定で、`:nth-child()`, `:nth-last-child()` というものがあります。
`:last-child` が「最後の要素」を指すのに対し、`:nth-child()` は「～番目の要素」、`:nth-last-child()` は「後ろから～番目の要素」を指定するための記述です。
たとえば、`:nth-child(3)` で「3番目の要素」、`:nth-last-child(3)` で「後ろから3番目の要素」に適用するスタイルを書くことができます。
それだけでなく、`:nth-child(3n)` で「3の倍数番目の要素」、`:nth-last-child(-n + 3)` で「後ろから1, 2, 3番目の要素」といった指定もできるので、今回はそれを使いましょう。

また、左右の余白を設定するので、グリッドの幅も `width: calc(100% / 3);` ではなく余白を考慮した指定に修正しましょう。

```html{31-33,36-42}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile-grid">
        <div class="tile-grid__item">
          <div class="tile">
            ...
          </div>
        </div>

        ...
      </div>
    </div>

    <style>
      /* ... */

      .tile-grid {
        display: flex;
        flex-wrap: wrap;
      }

      .tile-grid__item {
        width: calc((100% - var(--spacing-1) * 2) / 3);
        margin-right: var(--spacing-1);
        margin-bottom: var(--spacing-1);
      }

      .tile-grid__item:nth-child(3n) {
        margin-right: 0;
      }

      .tile-grid__item:nth-last-child(-n + 3) {
        margin-bottom: 0;
      }

      .tile__image {
        width: 100%;
      }
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-image-grid-styling-5.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

<!-- <img :src="$withBase('/layout-content-image-grid-styling-2.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef"> -->

最後に、footerとの間に余白を設定して完成です。

```html{11,15,29-31}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="tile-grid-area">
        <div class="tile-grid">
          ...
        </div>
      </div>
    </div>

    <style>
      /* ... */

      .card-column-area {
        margin-bottom: var(--spacing-5);
      }

      .media-list-area {
        margin-bottom: var(--spacing-5);
      }

      .tile-grid-area {
        margin-bottom: var(--spacing-5);
      }

      /* ... */
    </style>
  </body>
</html>
```

<img :src="$withBase('/layout-content-image-grid-styling-6.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

<a href="../../samples/layout-content-image-grid.html" target="_blank">ここまでの成果物</a>

## まとめ

- `position: relative;`、`position: absolute;` を指定することで、要素を重ねることができる
- `top`, `left`, `right`, `bottom` を使って、`position: absolute;` で重ねた要素の位置を指定できる
- `flex-wrap: wrap` を使うと、`display: flex;` によって横並びにした要素を折り返して複数行に表示できる
- `:nth-child()`, `:nth-last-child()` によって、「～番目の要素」や「後ろから～番目の要素」にスタイルを指定できる

::: tip
`display: flex;` とそれに関連するプロパティの挙動については次の記事が分かりやすいです。

[A Complete Guide to Flexbox | CSS-Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

また、次のサービスでコードを書きながら挙動を理解するのも良いでしょう。

[Flexbox Froggy - CSS flexbox 学習ゲーム](https://flexboxfroggy.com/#ja)
:::

::: tip
この章で新たに登場したCSSプロパティ、CSSセレクタは以下のとおりです。

- [position - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/position)
- [top - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/top)
- [left - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/left)
- [right - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/right)
- [bottom - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/bottom)
- [flex-wrap - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/flex-wrap)
- [:nth-child() - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/:nth-child)
- [:nth-last-child() - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/:nth-last-child)
  :::
