# メディアのリストをつくる

## 構造をイメージする

今回作るのは次のようなリストです。

<img :src="$withBase('/layout-content-media-list-image.png')" alt="実装するリストのイメージ" width="375" style="border: 1px solid #eaecef">

構成する四角形をイメージしてみてください。次のような3種類の構造が浮かぶでしょうか。

<img :src="$withBase('/layout-content-media-list-structure-1.png')" alt="実装するリストの構造のイメージ1" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-media-list-structure-2.png')" alt="実装するリストの構造のイメージ2" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-media-list-structure-3.png')" alt="実装するリストの構造のイメージ3" width="375" style="border: 1px solid #eaecef">

構造のみを表示すると次のようになります。

<img :src="$withBase('/layout-content-media-list-skeleton-1.png')" alt="実装するリストの構造を抽出したイメージ1" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-media-list-skeleton-2.png')" alt="実装するリストの構造を抽出したイメージ2" width="375" style="border: 1px solid #eaecef">

<img :src="$withBase('/layout-content-media-list-skeleton-3.png')" alt="実装するリストの構造を抽出したイメージ2" width="375" style="border: 1px solid #eaecef">

赤色の四角形の中にはオレンジ色の四角形が4つ縦に並んでいます。
緑色の四角形の中には水色の四角形が2つ横に並んでいます。このとき、左の水色の四角形は幅が決まっており、右の水色の四角形は残りの幅を全て埋めるようにレイアウトするとしましょう。
最後に、黄緑色の四角形の中には黄土色の四角形が2つ縦に並んでいます。黄土色の四角形は黄緑色の四角形の中で、上下の中央に配置されています。

## メディアのレイアウトを作る

画像とテキストが横並びになっているようなパーツのことをしばしばメディアと呼びます。

まずはメディアのひとつから作っていきます。緑色と水色の四角形と、黄緑色と黄土色の四角形の部分です。

HTMLを書くと次のようになります。

```html{11-25}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div>
        <div>
          <img src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div>
          <div>
            ポラーノの広場
          </div>

          <div>
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

まずは、水色の四角形を横並びにしましょう。

横並びにするには `display: flex;` を使い、伸びて欲しい子要素には `flex-grow: 1;`、縮んでほしくない要素には `flex-shrink: 0;` を指定すればよさそうです。

```html{11,12,16,32-42}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img src="../static/sample1.jpg" alt="サンプル" />
        </div>

        <div class="media__text-area">
          <div>
            ポラーノの広場
          </div>

          <div>
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media {
        display: flex;
      }

      .media__image-area {
        flex-shrink: 0;
      }

      .media__text-area {
        flex-grow: 1;
      }
    </style>
  </body>
</html>
```

ここまでで、次のような見た目になります。

<img :src="$withBase('/layout-content-media-list-styling-1.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

実装したものが変に見えるかもしれません。そのときはブラウザの幅を広げてみてください。意図した見た目になっていることが確認しやすくなります。
画像が幅をとりすぎているのが原因なので、画像の幅を制限しましょう。ここでは、基準となるサイズ `--spacing-unit` の12倍の幅とします。

```html{14,44-46}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div>
            ポラーノの広場
          </div>

          <div>
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media {
        display: flex;
      }

      .media__image-area {
        flex-shrink: 0;
      }

      .media__image {
        width: calc(var(--spacing-unit) * 12);
      }

      .media__text-area {
        flex-grow: 1;
      }
    </style>
  </body>
</html>
```

画像の幅を固定できました。

<img :src="$withBase('/layout-content-media-list-styling-2.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

次に、メディアの見出しと本文のスタイルを当てましょう。

```html{21,25,36-50}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading">
            ポラーノの広場
          </div>

          <div class="media__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media__heading {
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
      }

      .media__body {
        font-family: sans-serif;
        font-weight: normal;
        font-size: var(--font-size-small-2);
        line-height: var(--line-height-base);
        color: var(--color-black);
      }
    </style>
  </body>
</html>
```

次のような見た目になったでしょうか。

<img :src="$withBase('/layout-content-media-list-styling-3.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

## メディアのテキストのレイアウトを作る

続いて、構造をイメージした際の黄土色の四角形にあたる部分の位置調整をしていきましょう。

黄緑色の四角形には余白があり、黄土色の四角形は黄緑色の四角形のなかで上下の中央に2つ配置されています。

[footer のスタイリング](/layout-content/layout.md#css-を書く) で使ったように、要素を左右の中央に配置するには `display: flex;` と `justify-content: center;` という指定を使えばよかったのでした。
上下の中央に配置するにはこれに加えて、`flex-direction: column;` という指定をします。

`display: flex;` は子要素を横並びにしますが、`flex-direction: column;` と合わせて指定すると子要素を縦に並べるようになります。
`justify-content: center;` は `display: flex;` と組み合わせることで要素を左右中央に寄せる効果がありますが、`flex-direction: column;` と合わせて指定すると子要素を上下の中央に寄せることができます。

```html{38-41}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading">
            ポラーノの広場
          </div>

          <div class="media__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media__text-area {
        flex-grow: 1;
        padding: var(--spacing-1);
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      /* ... */
    </style>
  </body>
</html>
```

見出しと本文を上下の中央に配置し、周りに余白を設定できました。

<img :src="$withBase('/layout-content-media-list-styling-4.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

最後に、見出しと本文の間の余白と、メディア全体の背景色を設定しておきましょう。

```html{21,25,40,45-47}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading-area">
            <div class="media__heading">
              ポラーノの広場
            </div>
          </div>

          <div class="media__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media {
        display: flex;
        background: var(--color-gray-light);
      }

      /* ... */

      .media__heading-area {
        margin-bottom: var(--spacing-1);
      }

      .media__heading {
        font-family: serif;
        font-weight: bold;
        font-size: var(--font-size-large-1);
        line-height: var(--line-height-small);
        color: var(--color-black);
      }

      /* ... */
    </style>
  </body>
</html>
```

めでたくメディアレイアウトの完成です。

<img :src="$withBase('/layout-content-media-list-styling-5.png')" alt="ここまでの実装をブラウザで確認したときのキャプチャ" width="375" style="border: 1px solid #eaecef">

## メディアのリストを作る

まずはメディアを4つに増やしましょう。

```html{33-97}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading-area">
            <div class="media__heading">
              ポラーノの広場
            </div>
          </div>

          <div class="media__body">
            あのイーハトーヴォのすきとおった風
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading-area">
            <div class="media__heading">
              ポラーノの広場
            </div>
          </div>

          <div class="media__body">
            夏でも底に冷たさをもつ青いそら
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading-area">
            <div class="media__heading">
              ポラーノの広場
            </div>
          </div>

          <div class="media__body">
            うつくしい森で飾られたモリーオ市
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media__image-area">
          <img
            class="media__image"
            src="../static/sample1.jpg"
            alt="サンプル"
          />
        </div>

        <div class="media__text-area">
          <div class="media__heading-area">
            <div class="media__heading">
              ポラーノの広場
            </div>
          </div>

          <div class="media__body">
            郊外のぎらぎらひかる草の波
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

メディア同士の間に余白を設けましょう。
赤色の四角形の中にオレンジ色の四角形が4つ縦に並んでいる構造を作っていきます。

まずはメディアひとつひとつをdiv要素で囲みましょう。

```html{11,12,16,18,22,24,28,30,35,36}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media-list">
        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */
    </style>
  </body>
</html>
```

`class="media-list"` は無くてもかまいませんが、`class="media-list"` に対する `class="media-list__item"` という関係性を表現するために追加しています。

追加したdiv要素を使って余白をつけていきましょう。

いろいろと方法はありますが、今回は `class="media-list__item"` に `margin-bottom` を設定してみます。

このとき、4つめの `class="media-list__item"` にも下に余計な余白がついてしまいますが、`:last-child` という指定を使うことで最後の要素に余白がつかないようにできます。

```html{42-51}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      ...

      <div class="media-list">
        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>

        <div class="media-list__item">
          <div class="media">
            ...
          </div>
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .media-list {
      }

      .media-list__item {
        margin-bottom: var(--spacing-2);
      }

      .media-list__item:last-child {
        margin-bottom: 0;
      }

      .media {
        display: flex;
      }

      /* ... */
    </style>
  </body>
</html>
```

最後に、`class="card-column"` と `class="media-list"` の間に余白を設定し、全体の余白を調整すれば完成です。

```html{9,13,15,19,27-29,32-38}
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    ...

    <div class="main">
      <div class="card-column-area">
        <div class="card-column">
          ...
        </div>
      </div>

      <div class="media-list-area">
        <div class="media-list">
          ...
        </div>
      </div>
    </div>

    ...
    <style>
      /* ... */

      .main {
        padding-top: var(--spacing-5);
        padding-left: var(--spacing-2);
        padding-right: var(--spacing-2);
      }

      .card-column-area {
        margin-bottom: var(--spacing-5);
      }

      .media-list-area {
        margin-bottom: var(--spacing-5);
      }

      /* ... */
    </style>
  </body>
</html>
```

<a href="../../samples/layout-content-media-list.html" target="_blank">ここまでの成果物</a>

## まとめ

- `flex-direction: column;` によって、`display: flex;` が子要素を並べる向きを横から縦に変えることができる
- `:last-child` によって、最後の要素にスタイルを指定できる

::: tip
この章で新たに登場したCSSプロパティは以下のとおりです。

- [flex-direction - CSS: カスケーディングスタイルシート | MDN](https://developer.mozilla.org/ja/docs/Web/CSS/flex-direction)
  :::
